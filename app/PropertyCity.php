<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyCity extends Model
{
    //

    protected $fillable = [
        'property_province_id',
        'name',
        'zone_code',
    ];

    public function province()
    {
        return $this->belongsTo('App\PropertyProvince', 'property_province_id');
    }
}
