<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PropertyInsuranceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'logo' => $this->logo,
            'admin_fee' => $this->admin_fee,
            'created_at' => $this->created_at->format('Y-m-d H:i'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i'),
            'rates' => [
                'rate_flexas_class1' => $this->rate_flexas_class1,
                'rate_flexas_class2' => $this->rate_flexas_class1,
                'rate_flexas_class3' => $this->rate_flexas_class3,
                'rate_rsmdcc' => $this->rate_rsmdcc,
                'rate_other' => $this->rate_other,
                'rate_tsfwd' => $this->rate_tsfwd,
                'rate_eqvet_zone1' => $this->rate_eqvet_zone1,
                'rate_eqvet_zone2' => $this->rate_eqvet_zone2,
                'rate_eqvet_zone3' => $this->rate_eqvet_zone3,
                'rate_eqvet_zone4' => $this->rate_eqvet_zone4,
                'rate_eqvet_zone5' => $this->rate_eqvet_zone5,
            ]
        ];
    }
}
