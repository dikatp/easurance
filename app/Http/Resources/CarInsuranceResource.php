<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CarInsuranceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $rates = collect([
            'comp' => collect([
                '1' => collect(),
                '2' => collect(),
                '3' => collect(),
            ]),
            'tlo' => collect([
                '1' => collect(),
                '2' => collect(),
                '3' => collect(),
            ])
        ]);

        foreach ($this->base_rates as $rate) {
            $rates[$rate->type][$rate->region]->put($rate->category, number_format($rate->value, 2, '.', ''));
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'logo' => $this->logo,
            'admin_fee' => $this->admin_fee,
            'created_at' => $this->created_at->format('Y-m-d H:i'),
            'updated_at' => $this->updated_at->format('Y-m-d H:i'),
            'rates' => $rates,
            'rates_add' => [
                'tsfwd_comp' => $this->tsfwd_comp,
                'tsfwd_tlo' => $this->tsfwd_tlo,
                'eqvet_comp' => $this->eqvet_comp,
                'eqvet_tlo' => $this->eqvet_tlo,
                'srcc_comp' => $this->srcc_comp,
                'srcc_tlo' => $this->srcc_tlo,
                'ts_comp' => $this->ts_comp,
                'ts_tlo' => $this->ts_tlo,
                'tpl' => $this->tpl,
                'pa_driver' => $this->pa_driver,
                'pa_passenger' => $this->pa_passenger,
                'atpm' => $this->atpm,
            ]
        ];
    }
}
