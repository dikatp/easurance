<?php

namespace App\Http\Controllers\Admin;

use App\CarPlate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarPlateController extends Controller
{
    public function index()
    {
        $plates = CarPlate::all();
        return response()->json($plates);
    }

    public function show($id)
    {
        $plate = CarPlate::findOrFail($id);
        return response()->json($plate);
    }

    public function store(Request $request)
    {
        $request->validate([
            'plate' => 'required|unique:car_plates',
            'region_code' => 'required|numeric'
        ]);

        $plate = new CarPlate();
        $plate->plate = $request->plate;
        $plate->region_code = $request->region_code;
        $plate->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'plate' => [
                'required',
                Rule::unique('car_plates')->ignore($id)
            ],
            'region_code' => 'required'
        ]);

        $plate = CarPlate::findOrFail($id);
        $plate->plate = $request->plate;
        $plate->region_code = $request->region_code;
        $plate->save();

        return response()->json(['message' => 'Update Succeed']);
    }
    public function destroy($id)
    {
        $plate = CarPlate::findOrFail($id);
        $plate->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }
}
