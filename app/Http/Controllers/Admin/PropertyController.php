<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PropertyProvince;
use Illuminate\Validation\Rule;
use App\PropertyCity;
use App\Http\Resources\PropertyProvinceResource;

/**
 * Controller for:
 * - property cities table
 * - property provinces table
 */

class PropertyController extends Controller
{
    //
    public function provinceIndex()
    {
        $provinces = PropertyProvince::all();

        return response()->json($provinces);
    }

    public function provinceShow($id)
    {
        $province = PropertyProvince::with('cities')->findOrFail($id);

        return response()->json(PropertyProvinceResource::make($province));
    }

    public function provinceStore(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:property_provinces',
        ]);

        $province = new PropertyProvince();
        $province->name = $request->name;
        $province->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function provinceUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('property_provinces')->ignore($id)
            ]
        ]);

        $province = PropertyProvince::findOrFail($id);
        $province->name = $request->name;
        $province->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function provinceDestroy($id)
    {
        $province = PropertyProvince::findOrFail($id);
        $province->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }

    public function cityIndex($province_id)
    {
        $cities = PropertyCity::where('property_province_id', $province_id)->orderBy('name', 'asc')->get();

        return response()->json($cities);
    }

    public function cityShow($id)
    {
        $city = PropertyCity::findOrFail($id);

        return response()->json($city);
    }

    public function cityStore(Request $request, $province_id)
    {
        $request->validate([
            'name' => 'required|unique:property_cities',
            'zone_code' => 'required|numeric'
        ]);

        $province = PropertyProvince::findOrFail($province_id);
        $province->cities()->create([
            'name' => $request->name,
            'zone_code' => $request->zone_code
        ]);

        return response()->json(['message' => 'Create Succeed']);
    }

    public function cityUpdate(Request $request, $province_id, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('property_cities')->ignore($id)
            ],
            'zone_code' => 'required|numeric'
        ]);

        $city = PropertyCity::findOrFail($id);
        $city->name = $request->name;
        $city->zone_code = $request->zone_code;
        $city->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function cityDestroy($province_id, $id)
    {
        $city = PropertyCity::findOrFail($id);
        $city->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }
}
