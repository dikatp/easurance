<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;
use Illuminate\Validation\Rule;

/**
 * Controller for faqs table
 */

class FaqController extends Controller
{
    //
    public function index()
    {
        $faqs = Faq::all();

        return response()->json($faqs);
    }

    public function show($id)
    {
        $faq = Faq::findOrFail($id);

        return response()->json($faq);
    }

    public function store(Request $request)
    {
        $request->validate([
            'question' => 'required|unique:faqs',
            'answer' => 'required'
        ]);

        $faq = new Faq();
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'question' => [
                'required',
                Rule::unique('faqs')->ignore($id)
            ],
            'answer' => 'required'
        ]);

        $faq = Faq::findOrFail($id);
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->save();

        return response()->json(['message' => 'Update Succeed']);
    }
    public function destroy($id)
    {
        $faq = Faq::findOrFail($id);
        $faq->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }
}
