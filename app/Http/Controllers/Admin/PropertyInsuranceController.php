<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PropertyInsurance;
use App\PropertyBenefit;
use App\Http\Resources\PropertyInsuranceResource;
use App\Http\Resources\BasicResource;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

/**
 * Controller for:
 * - property insurances table
 */

class PropertyInsuranceController extends Controller
{
    //
    public function insuranceIndex()
    {
        $insurances = PropertyInsurance::all();

        return response()->json(BasicResource::collection($insurances));
    }

    public function insuranceShow($id)
    {
        $insurance = PropertyInsurance::findOrFail($id);

        return response()->json(PropertyInsuranceResource::make($insurance));
    }

    public function insuranceStore(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:property_insurances',
            'logo' => 'required',
            'admin_fee' => 'required|numeric',
            // 'rate_flexas_class1' => 'required|numeric',
            // 'rate_flexas_class2' => 'required|numeric',
            // 'rate_flexas_class3' => 'required|numeric',
            // 'rate_rsmdcc' => 'required|numeric',
            // 'rate_other' => 'required|numeric',
            // 'rate_tsfwd' => 'required|numeric',
            // 'rate_eqvet_zone1' => 'required|numeric',
            // 'rate_eqvet_zone2' => 'required|numeric',
            // 'rate_eqvet_zone3' => 'required|numeric',
            // 'rate_eqvet_zone4' => 'required|numeric',
            // 'rate_eqvet_zone5' => 'required|numeric',
        ]);

        $insurance = new PropertyInsurance($request->rates);
        $insurance->name = $request->name;
        $insurance->admin_fee = $request->admin_fee;

        if ($request->has('logo')) {
            $image = $request->input('logo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'logo-' . Str::slug($insurance->name) . '.' . $image_extension[1];
            $path = '/media/image/partner/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $insurance->logo = $path . $name;
        }

        $insurance->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function insuranceUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('property_insurances')->ignore($id)
            ],
            // 'logo' => 'required',
            // 'admin_fee' => 'required|numeric',
            // 'rate_flexas_class1' => 'required|numeric',
            // 'rate_flexas_class2' => 'required|numeric',
            // 'rate_flexas_class3' => 'required|numeric',
            // 'rate_rsmdcc' => 'required|numeric',
            // 'rate_other' => 'required|numeric',
            // 'rate_tsfwd' => 'required|numeric',
            // 'rate_eqvet_zone1' => 'required|numeric',
            // 'rate_eqvet_zone2' => 'required|numeric',
            // 'rate_eqvet_zone3' => 'required|numeric',
            // 'rate_eqvet_zone4' => 'required|numeric',
            // 'rate_eqvet_zone5' => 'required|numeric',
        ]);

        $insurance = PropertyInsurance::findOrFail($id);
        $insurance->name = $request->name;
        $insurance->admin_fee = $request->admin_fee;
        $insurance->update($request->rates);

        if ($request->has('logo')) {
            //File::delete(public_path($insurance->logo_path));
            $image = $request->input('logo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'logo-' . Str::slug($insurance->name) . '.' . $image_extension[1];
            $path = '/media/image/partner/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $insurance->logo = $path . $name;
        }

        $insurance->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function insuranceDestroy($id)
    {
        $insurance = PropertyInsurance::findOrFail($id);
        $insurance->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }

    public function insuranceBenefitIndex($id)
    {
        $benefit = PropertyInsurance::find($id)->benefits;
        return response()->json($benefit);
    }

    public function insuranceBenefitSync(Request $request, $id)
    {
        $insurance = PropertyInsurance::find($id);
        $insurance->benefits()->sync($request->benefits);
        return response()->json(['message' => 'insurance benefit sync success!']);
    }
}
