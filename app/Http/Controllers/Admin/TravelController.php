<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TravelDestination;
use Illuminate\Validation\Rule;

/**
 * Controller for:
 * - travel destinations table
 */

class TravelController extends Controller
{
    //
    public function index()
    {
        $travels = TravelDestination::all();

        return response()->json($travels);
    }

    public function show($id)
    {
        $travel = TravelDestination::findOrFail($id);

        return response()->json($travel);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:travel_destinations',
            'type' => 'required',
        ]);

        $travel = new TravelDestination();
        $travel->name = $request->name;
        $travel->type = $request->type;
        $travel->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('travel_destinations')->ignore($id)
            ],
            'type' => 'required'
        ]);

        $travel = TravelDestination::findOrFail($id);
        $travel->name = $request->name;
        $travel->type = $request->type;
        $travel->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function destroy($id)
    {
        $travel = TravelDestination::findOrFail($id);
        $travel->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }
}
