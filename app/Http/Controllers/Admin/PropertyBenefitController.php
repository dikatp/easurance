<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PropertyBenefit;
use Illuminate\Validation\Rule;

class PropertyBenefitController extends Controller
{
    //

    public function index()
    {
        $benefits = PropertyBenefit::all();

        return response()->json($benefits);
    }

    public function show($id)
    {
        $benefit = PropertyBenefit::findOrFail($id);

        return response()->json($benefit);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:property_benefits',
        ]);

        $benefit = new PropertyBenefit();
        $benefit->name = $request->name;
        $benefit->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('property_benefits')->ignore($id)
            ]
        ]);

        $benefit = PropertyBenefit::findOrFail($id);
        $benefit->name = $request->name;
        $benefit->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function destroy($id)
    {
        $benefit = PropertyBenefit::findOrFail($id);
        $benefit->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }
}
