<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Article;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

/**
 * Controller for articles table
 */

class ArticleController extends Controller
{
    //
    public function index()
    {
        $articles = Article::all();
        return response()->json($articles);
    }

    public function show($id)
    {
        $article = Article::findOrFail($id);
        return response()->json($article);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:articles',
            // 'thumbnail' => 'required',
            'content' => 'required'
        ]);

        $article = new Article();
        $article->title = $request->title;
        $article->content = $request->content;

        if ($request->has('thumbnail')) {
            //File::delete(public_path($insurance->logo_path));
            $image = $request->input('thumbnail');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'thumbnail-' . Str::slug($article->name) . '.' . $image_extension[1];
            $path = '/media/image/article/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $article->thumbnail = $path . $name;
        }

        $article->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => [
                'required',
                Rule::unique('articles')->ignore($id)
            ],
            // 'thumbnail' => 'required',
            'content' => 'required'
        ]);

        $article = Article::findOrFail($id);
        $article->title = $request->title;
        $article->content = $request->content;

        if ($request->has('thumbnail')) {
            //File::delete(public_path($insurance->logo_path));
            $image = $request->input('thumbnail');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'thumbnail-' . Str::slug($article->name) . '.' . $image_extension[1];
            $path = '/media/image/article/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $article->thumbnail = $path . $name;
        }

        $article->save();

        return response()->json(['message' => 'Update Succeed']);
    }
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }
}
