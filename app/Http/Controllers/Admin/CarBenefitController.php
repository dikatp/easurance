<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CarBenefit;
use Illuminate\Validation\Rule;

class CarBenefitController extends Controller
{
    //
    public function index()
    {
        $benefits = CarBenefit::all();

        return response()->json($benefits);
    }

    public function show($id)
    {
        $benefit = CarBenefit::findOrFail($id);

        return response()->json($benefit);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:car_benefits',
        ]);

        $benefit = new CarBenefit();
        $benefit->name = $request->name;
        $benefit->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('car_benefits')->ignore($id)
            ]
        ]);

        $benefit = CarBenefit::findOrFail($id);
        $benefit->name = $request->name;
        $benefit->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function destroy($id)
    {
        $benefit = CarBenefit::findOrFail($id);
        $benefit->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }
}
