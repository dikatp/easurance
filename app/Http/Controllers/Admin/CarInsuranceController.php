<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\BasicResource;
use App\Http\Resources\CarInsuranceResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CarInsurance;
use Illuminate\Validation\Rule;
use App\CarBenefit;
use App\CarWorkshop;
use App\CarBaseRate;
use Illuminate\Support\Str;

/**
 * Controller for:
 * - car insurances table
 * - car workshops table
 * - car add rates table
 * - car base rates table
 */

class CarInsuranceController extends Controller
{
    //
    public function insuranceIndex()
    {
        $insurances = CarInsurance::all();

        return response()
            ->json(
                BasicResource::collection($insurances)
            );
    }

    public function insuranceShow($id)
    {
        $insurance = CarInsurance::with('base_rates')->findOrFail($id);

        return response()
            ->json(
                CarInsuranceResource::make($insurance)
            );
    }

    public function insuranceStore(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:car_insurances',
            'logo' => 'required',
            'admin_fee' => 'required|numeric',
        ]);

        $insurance = new CarInsurance($request->rates_add);
        $insurance->name = $request->name;
        $insurance->admin_fee = $request->admin_fee;

        if ($request->has('logo')) {
            //File::delete(public_path($insurance->logo_path));
            $image = $request->input('logo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'logo-' . Str::slug($insurance->name) . '.' . $image_extension[1];
            $path = '/media/image/partner/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $insurance->logo = $path . $name;
        }

        $insurance->save();

        $rates = [];
        foreach ($request->rates as $type => $i) {
            foreach ($i as $region => $j) {
                foreach ($j as $category => $value) {
                    array_push($rates, [
                        'type' => $type,
                        'region' => $region,
                        'category' => $category,
                        'value' => $value,
                    ]);
                }
            }
        }

        $insurance->base_rates()->createMany($rates);

        return response()->json(['message' => 'Create Succeed']);
    }

    public function insuranceUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('car_insurances')->ignore($id)
            ],
        ]);

        $insurance = CarInsurance::findOrFail($id);
        $insurance->name = $request->name;
        $insurance->admin_fee = $request->admin_fee;
        $insurance->update($request->rates_add);

        if ($request->has('logo')) {
            //File::delete(public_path($insurance->logo_path));
            $image = $request->input('logo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'logo-' . Str::slug($insurance->name) . '.' . $image_extension[1];
            $path = '/media/image/partner/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $insurance->logo = $path . $name;
        }
        $insurance->save();

        foreach ($request->rates as $type => $i) {
            foreach ($i as $region => $j) {
                foreach ($j as $category => $value) {
                    $insurance->base_rates()->where(
                        [
                            'type' => $type,
                            'region' => $region,
                            'category' => $category,
                        ]
                    )->update(
                        [
                            'value' => $value
                        ]
                    );
                }
            }
        }


        return response()->json(['message' => 'Update Succeed']);
    }

    public function insuranceDestroy($id)
    {
        $insurance = CarInsurance::findOrFail($id);
        $insurance->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }

    public function baseRateIndex($insurance_id)
    {
        $base_rates = CarBaseRate::where('car_insurance_id', $insurance_id)->orderBy('name', 'asc')->get();

        return response()->json($base_rates);
    }

    public function baseRateShow($id)
    {
        $base_rate = CarBaseRate::findOrFail($id);

        return response()->json($base_rate);
    }

    public function baseRateStore(Request $request, $insurance_id)
    {
        $request->validate([
            'type' => 'required',
            'category' => 'required|numeric',
            'region' => 'required|numeric',
            'price' => 'required|numeric',
        ]);

        $insurance = CarInsurance::findOrFail($insurance_id);
        $insurance->base_rates()->create([
            'type' => $request->type,
            'category' => $request->category,
            'region' => $request->region,
            'price' => $request->price,
        ]);

        return response()->json(['message' => 'Create Succeed']);
    }

    public function baseRateUpdate(Request $request, $id)
    {
        $request->validate([
            'type' => 'required',
            'category' => 'required|numeric',
            'region' => 'required|numeric',
            'price' => 'required|numeric',
        ]);

        $add_rate = CarBaseRate::findOrFail($id);
        $add_rate->name = $request->name;
        $add_rate->category = $request->category;
        $add_rate->region = $request->region;
        $add_rate->price = $request->price;
        $add_rate->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function baseRateDestroy($id)
    {
        $add_rate = CarBaseRate::findOrFail($id);
        $add_rate->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }

    public function insuranceBenefitIndex($id)
    {
        $benefit = CarInsurance::find($id)->benefits;
        return response()->json($benefit);
    }

    public function insuranceBenefitSync(Request $request, $id)
    {
        $insurance = CarInsurance::find($id);
        $insurance->benefits()->sync($request->benefits);
        return response()->json(['message' => 'insurance benefit sync success!']);
    }

    public function insuranceWorkshopIndex($id)
    {
        $workshop = CarInsurance::find($id)->workshops;
        return response()->json($workshop);
    }

    public function insuranceWorkshopSync(Request $request, $id)
    {
        $insurance = CarInsurance::find($id);
        $insurance->workshops()->sync($request->workshops);
        return response()->json(['message' => 'insurance workshop sync success!']);
    }
}
