<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TravelInsurance;
use App\TravelBenefit;
use App\TravelPackage;
use App\TravelRate;
use App\Http\Resources\TravelInsuranceResource;
use App\Http\Resources\BasicResource;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

/**
 * Controller for:
 * - travel insurances table
 * - travel packages table
 * - travel rates table
 */

class TravelInsuranceController extends Controller
{
    //
    public function insuranceIndex()
    {
        $insurances = TravelInsurance::all();

        return response()->json(BasicResource::collection($insurances));
    }

    public function insuranceShow($id)
    {
        $insurance = TravelInsurance::with('packages.rates')->findOrFail($id);

        return response()->json(TravelInsuranceResource::make($insurance));
    }

    public function insuranceStore(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:travel_insurances',
            'logo' => 'required',
            'admin_fee' => 'required|numeric'
        ]);

        $insurance = new TravelInsurance();
        $insurance->name = $request->name;
        // $insurance->logo = $request->logo;
        $insurance->admin_fee = $request->admin_fee;

        if ($request->has('logo')) {
            $image = $request->input('logo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'logo-' . Str::slug($insurance->name) . '.' . $image_extension[1];
            $path = '/media/image/partner/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $insurance->logo = $path . $name;
        }

        $insurance->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function insuranceUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('travel_insurances')->ignore($id)
            ],
            // 'logo' => 'required',
            // 'admin_fee' => 'required|numeric'
        ]);

        $insurance = TravelInsurance::findOrFail($id);
        $insurance->name = $request->name;
        $insurance->admin_fee = $request->admin_fee;

        if ($request->has('logo')) {
            //File::delete(public_path($insurance->logo_path));
            $image = $request->input('logo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'logo-' . Str::slug($insurance->name) . '.' . $image_extension[1];
            $path = '/media/image/partner/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $insurance->logo = $path . $name;
        }

        $insurance->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function insuranceDestroy($id)
    {
        $insurance = TravelInsurance::findOrFail($id);
        $insurance->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }

    public function packageIndex($insurance_id)
    {
        $packages = TravelPackage::where('travel_insurance_id', $insurance_id)->orderBy('name', 'asc')->get();

        return response()->json($packages);
    }

    public function packageShow($id)
    {
        $package = TravelPackage::findOrFail($id);

        return response()->json($package);
    }

    public function packageStore(Request $request, $insurance_id)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'region' => 'required'
        ]);

        $insurance = TravelInsurance::findOrFail($insurance_id);
        $insurance->packages()->create([
            'name' => $request->name,
            'type' => $request->type,
            'region' => $request->region
        ]);

        return response()->json(['message' => 'Create Succeed']);
    }

    public function packageUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'region' => 'required'
        ]);

        $package = TravelPackage::findOrFail($id);
        $package->name = $request->name;
        $package->type = $request->type;
        $package->region = $request->region;
        $package->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function packageDestroy($insurance_id, $id)
    {
        $package = TravelPackage::findOrFail($id);
        $package->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }

    public function rateIndex($package_id)
    {
        $rates = TravelRate::where('travel_package_id', $package_id)->orderBy('duration_from', 'asc')->get();

        return response()->json($rates);
    }

    public function rateShow($id)
    {
        $rate = TravelRate::findOrFail($id);

        return response()->json($rate);
    }

    public function rateStore(Request $request, $package_id)
    {
        $request->validate([
            'duration_from' => 'required|numeric',
            'duration_to' => 'required|numeric',
            'price' => 'required|numeric',
        ]);

        $package = TravelPackage::findOrFail($package_id);
        $package->rates()->create([
            'duration_from' => $request->duration_from,
            'duration_to' => $request->duration_to,
            'price' => $request->price
        ]);

        return response()->json(['message' => 'Create Succeed']);
    }

    public function rateUpdate(Request $request, $id)
    {
        $request->validate([
            'duration_from' => 'required|numeric',
            'duration_to' => 'required|numeric',
            'price' => 'required|numeric',
        ]);

        $rate = TravelRate::findOrFail($id);
        $rate->duration_from = $request->duration_from;
        $rate->duration_to = $request->duration_to;
        $rate->price = $request->price;
        $rate->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function rateDestroy($package_id, $id)
    {
        $rate = TravelRate::findOrFail($id);
        $rate->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }

    public function insuranceBenefitIndex($id)
    {
        $benefit = TravelInsurance::find($id)->benefits;
        return response()->json($benefit);
    }

    public function insuranceBenefitSync(Request $request, $id)
    {
        $insurance = TravelInsurance::find($id);
        $insurance->benefits()->sync($request->benefits);
        return response()->json(['message' => 'insurance benefit sync success!']);
    }
}
