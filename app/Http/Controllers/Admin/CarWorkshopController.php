<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CarWorkshop;
use Illuminate\Validation\Rule;

class CarWorkshopController extends Controller
{
    //
    public function index()
    {
        $workshops = CarWorkshop::all();

        return response()->json($workshops);
    }

    public function show($id)
    {
        $workshop = CarWorkshop::findOrFail($id);

        return response()->json($workshop);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required',
        ]);

        $workshop = new CarWorkshop();
        $workshop->name = $request->name;
        $workshop->address = $request->address;
        $workshop->city = $request->city;
        $workshop->phone = $request->phone;
        $workshop->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required',
        ]);

        $workshop = CarWorkshop::findOrFail($id);
        $workshop->name = $request->name;
        $workshop->address = $request->address;
        $workshop->city = $request->city;
        $workshop->phone = $request->phone;
        $workshop->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function destroy($id)
    {
        $workshop = CarWorkshop::findOrFail($id);
        $workshop->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }
}
