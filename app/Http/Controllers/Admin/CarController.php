<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\CarBrand;
use App\CarSeries;
use App\CarType;
use App\CarPrice;
use App\Http\Resources\CarSeriesResource;

/**
 * Controller for:
 * - car brands table
 * - car types table
 * - car series table
 * - car prices table
 * - car plates table
 */

class CarController extends Controller
{
    //
    public function brandIndex()
    {
        $brands = CarBrand::all();

        return response()->json($brands);
    }

    public function brandShow($id)
    {
        $brand = CarBrand::findOrFail($id);

        return response()->json($brand);
    }

    public function brandStore(Request $request)
    {
        if (!$request->name) {
            if (CarBrand::where('name', 'New Car Brand')->first()) {
                return response()->json(['message' => 'New car brand already exist! please edit it first.'], 403);
            } else {
                $name = 'New Car Brand';
            }
        } else {
            $name = $request->name;
        }
        CarBrand::create([
            'name' => $name,
        ]);
        return response()->json(['message' => 'New car brand data created!']);
    }

    public function brandUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string'
        ]);
        $brand = CarBrand::findOrFail($id);
        $brand->name = $request->name;
        $brand->save();
        return response()->json(['message' => 'Brand name change success!']);
    }

    public function brandDestroy($id)
    {
        $brand = CarBrand::findOrFail($id);
        $brand->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }

    public function typeIndex($brand_id)
    {
        $types = CarType::where('car_brand_id', $brand_id)->orderBy('name', 'asc')->get();

        return response()->json($types);
    }

    public function typeShow($id)
    {
        $type = CarType::findOrFail($id);

        return response()->json($type);
    }

    public function typeStore(Request $request, $brand_id)
    {
        $brand = CarBrand::findOrFail($brand_id);
        if (!$request->name) {
            if ($brand->types()->where('name', 'New Type')->first()) {
                return response()->json(['message' => 'New type already exist! please edit it first.'], 403);
            } else {
                $name = 'New Type';
            }
        } else {
            $name = $request->name;
        }
        $brand->types()->create([
            'name' => $name,
        ]);
        return response()->json(['message' => 'New type data created!']);
    }

    public function typeUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string'
        ]);
        $type = CarType::findOrFail($id);
        $type->name = $request->name;
        $type->save();
        return response()->json(['message' => 'Type name change success!']);
    }

    public function typeDestroy($id)
    {
        $type = CarType::findOrFail($id);
        $type->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }

    public function seriesIndex($type_id)
    {
        $series = CarSeriesResource::collection(CarSeries::with('prices')->where('car_type_id', $type_id)->orderBy('name', 'asc')->get());
        return response()->json($series);
    }

    public function seriesShow($id)
    {
        $series = CarSeries::findOrFail($id);

        return response()->json($series);
    }

    public function seriesStore(Request $request, $type_id)
    {
        $type = CarType::findOrFail($type_id);
        if (!$request->name) {
            if ($type->series()->where('name', 'New Series')->first()) {
                return response()->json(['message' => 'New series already exist! please edit it first.'], 403);
            } else {
                $name = 'New Series';
            }
        } else {
            $name = $request->name;
        }
        $type->series()->create([
            'name' => $name,
        ]);
        return response()->json(['message' => 'New series data created!']);
    }

    public function seriesUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string'
        ]);
        $series = CarSeries::findOrFail($id);
        $series->name = $request->name;
        $series->save();
        return response()->json(['message' => 'Series name change success!']);
    }

    public function seriesDestroy($id)
    {
        $series = CarSeries::findOrFail($id);
        $series->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }

    public function priceIndex($series_id)
    {
        $prices = CarPrice::where('car_series_id', $series_id)->orderBy('year', 'asc')->get();

        return response()->json($prices);
    }

    public function priceShow($id)
    {
        $price = CarPrice::findOrFail($id);

        return response()->json($price);
    }

    public function priceStore(Request $request, $series_id)
    {
        $request->validate([
            'year' => 'required',
            'price' => 'required|numeric',
        ]);

        $series = CarSeries::findOrFail($series_id);
        $series->prices()->updateOrCreate(
            ['year' => $request->year,],
            ['price' => $request->price]
        );

        return response()->json(['message' => 'Create Succeed']);
    }

    public function seriesPriceDestroy(Request $request, $series_id){
        $request->validate([
            'year' => 'required',
        ]);

        $series = CarSeries::findOrFail($series_id);
        $series->price($request->year)->delete();
        return response()->json(['message' => 'Delete Succeed']);

    }

    public function priceUpdate(Request $request, $id)
    {
        $request->validate([
            'year' => [
                'required',
                Rule::unique('car_prices')->ignore($id),
            ],
            'price' => 'required|numeric',
        ]);

        $price = CarPrice::findOrFail($id);
        $price->year = $request->year;
        $price->price = $request->price;
        $price->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function priceDestroy($id)
    {
        $price = CarPrice::findOrFail($id);
        $price->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }


}
