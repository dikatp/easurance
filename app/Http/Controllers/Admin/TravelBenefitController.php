<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TravelBenefit;
use Illuminate\Validation\Rule;

class TravelBenefitController extends Controller
{
    //


    public function index()
    {
        $benefits = TravelBenefit::all();

        return response()->json($benefits);
    }

    public function show($id)
    {
        $benefit = TravelBenefit::findOrFail($id);

        return response()->json($benefit);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:travel_benefits',
        ]);

        $benefit = new TravelBenefit();
        $benefit->name = $request->name;
        $benefit->save();

        return response()->json(['message' => 'Create Succeed']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('travel_benefits')->ignore($id)
            ]
        ]);

        $benefit = TravelBenefit::findOrFail($id);
        $benefit->name = $request->name;
        $benefit->save();

        return response()->json(['message' => 'Update Succeed']);
    }

    public function destroy($id)
    {
        $benefit = TravelBenefit::findOrFail($id);
        $benefit->delete();

        return response()->json(['message' => 'Delete Succeed']);
    }
}
