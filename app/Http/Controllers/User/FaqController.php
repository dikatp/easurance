<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;

class FaqController extends Controller
{
    //
    public function index()
    {
        $faqs = Faq::all();
        return response()->json($faqs);
    }
}
