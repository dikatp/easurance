<?php

namespace App\Http\Controllers\User;

use App\CarWishlist;
use App\Http\Controllers\Controller;
use App\PropertyWishlist;
use App\TravelWishlist;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getCarWishlist()
    {
        return response()->json(auth()->user()->carWishlists);
    }
    public function getPropertyWishlist()
    {
        return response()->json(auth()->user()->propertyWishlists);
    }
    public function getTravelWishlist()
    {
        return response()->json(auth()->user()->travelWishlists);
    }

    public function postCarWishlist(Request $request)
    {
        $request->validate([
            'premium.insurance_name' => 'required|string',
            'car_name' => 'required|string',
            'car_year' => 'required|numeric',
            'insurance_type' => 'required|string',
            'premium.prices.*.name' => 'required|string',
            'premium.prices.*.value' => 'required|numeric',
            'premium.total_price' => 'required|numeric',
        ]);

        $data = $request->only([
            'car_name',
            'car_year',
        ]);
        switch ($request->insurance_type) {
            case 'comp':
                $data['insurance_type'] = 'Comprehensive';
                break;
            case 'tlo':
                $data['insurance_type'] = 'Total Loss Only';
                break;
            default:
                return response()->json(['message' => 'insurance_type must be \'comp\' or \'tlo\''], 422);
        }
        $data['insurance_name'] = $request->premium['insurance_name'];
        $data['prices'] = $request->premium['prices'];
        $data['total_price'] = $request->premium['total_price'];

        $user = auth()->user();
        $wishlist = $user->carWishlists()->create($data);
        return response()->json($wishlist);
    }

    public function postPropertyWishlist(Request $request)
    {
        $request->validate([
            'premium.insurance_name' => 'required|string',
            'province' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string',
            'value' => 'required|numeric',
            'premium.prices.*.name' => 'required|string',
            'premium.prices.*.value' => 'required|numeric',
            'premium.total_price' => 'required|numeric',
        ]);

        $data = $request->only([
            'province',
            'city',
            'address',
            'value',
        ]);
        $data['insurance_name'] = $request->premium['insurance_name'];
        $data['prices'] = $request->premium['prices'];
        $data['total_price'] = $request->premium['total_price'];

        $user = auth()->user();
        $wishlist = $user->propertyWishlists()->create($data);
        return response()->json($wishlist);
    }

    public function postTravelWishlist(Request $request)
    {
        $request->validate([
            'premium.insurance_name' => 'required|string',
            'premium.package' => 'required|string',
            'destination' => 'required|string',
            'day' => 'required|numeric',
            'premium.prices.*.name' => 'required|string',
            'premium.prices.*.value' => 'required|numeric',
            'premium.total_price' => 'required|numeric',
        ]);

        $data = $request->only([
            'destination',
            'day',
        ]);
        $data['insurance_name'] = $request->premium['insurance_name'];
        $data['package_name'] = $request->premium['package'];
        $data['prices'] = $request->premium['prices'];
        $data['total_price'] = $request->premium['total_price'];
        $user = auth()->user();
        $wishlist = $user->travelWishlists()->create($data);
        return response()->json($wishlist);
    }

    public function destroyCarWishlist($id)
    {
        $wishlist = CarWishlist::findOrFail($id);
        if ($wishlist->user_id == auth()->id()) {
            return response()->json($wishlist->delete());
        }
        return response()->json(['message' => 'Not Allowed!'], 403);
    }

    public function destroyPropertyWishlist($id)
    {
        $wishlist = PropertyWishlist::findOrFail($id);
        if ($wishlist->user_id == auth()->id()) {
            return response()->json($wishlist->delete());
        }
        return response()->json(['message' => 'Not Allowed!'], 403);
    }

    public function destroyTravelWishlist($id)
    {
        $wishlist = TravelWishlist::findOrFail($id);
        if ($wishlist->user_id == auth()->id()) {
            return response()->json($wishlist->delete());
        }
        return response()->json(['message' => 'Not Allowed!'], 403);
    }
}
