<?php

namespace App\Http\Controllers\User;

use App\CarInsurance;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CarBrand;
use App\CarType;
use App\CarSeries;
use App\CarPlate;
use App\CarBenefit;

class CarController extends Controller
{
    //
    public function brandIndex($year)
    {
        $brands = CarBrand::whereHas('types.series.prices', function ($query) use ($year) {
            $query->where('year', $year);
        })->orderBy('name', 'asc')->get();
        if ($brands->count() > 0) {
            return response()->json($brands);
        }
        return response()->json(['message' => 'no brand found for year ' . $year], 404);
    }

    public function typeIndex($brand_id)
    {
        $types = CarType::where('car_brand_id', $brand_id)->orderBy('name', 'asc')->get();

        return response()->json($types);
    }

    public function seriesIndex($type_id)
    {
        $series = CarSeries::where('car_type_id', $type_id)->orderBy('name', 'asc')->get();

        return response()->json($series);
    }

    public function plateIndex()
    {
        $plates = CarPlate::all();
        return response()->json($plates);
    }

    /**
     * Request:
     * car_series_id - int
     * car_plate_id - string
     * car_year - string / int
     * insurance_type - string (comp / tlo)
     * additional[] - array of string
     * tpl_value - int
     * pa_driver_value - int
     * pa_passenger_value - int
     * pa_passenger_seats - int
     *
     * @param  Request  $request
     * @return
     * */
    public function calculatePremium(Request $request)
    {
        $request->validate([
            'car_series_id' => 'required',
            'car_plate_id' => 'required',
            'car_year' => 'required',
            'insurance_type' => 'required',
        ]);

        //get car series
        try {
            $carPrice = CarSeries::findOrFail($request->car_series_id)->price(intval($request->car_year));
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Car series not found!'], 400);
        }

        if ($carPrice->price > 800000000) $c = 5;
        elseif ($carPrice->price > 400000000) $c = 4;
        elseif ($carPrice->price > 200000000) $c = 3;
        elseif ($carPrice->price > 125000000) $c = 2;
        else $c = 1;

        $category = $c;

        //get region from plate
        try {
            $region = CarPlate::findOrFail($request->car_plate_id)->region_code;
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'vehicle_plate not found!'], 400);
        }

        //parse additional data
        $additionalParse = $this->additionalParse($request);
        if ($additionalParse['status'] != 200)
            return response()->json(['message' => $additionalParse['message']], $additionalParse['status']);
        $additional = $additionalParse['additional'];
        $additionalValue = $additionalParse['value'];

        $insurances = CarInsurance::with(['benefits'])->withCount('workshops')->get();


        $insurance_type = $request->insurance_type;
        if ($insurance_type == 'comp' || $insurance_type == 'tlo') {
            //Premium calculation
            $result = collect();

            foreach ($insurances as $insurance) {
                $baseRate = $insurance->base_rates()->where([
                    ['type', $insurance_type],
                    ['category', $category],
                    ['region', $region]
                ])->first();
                if (is_null($baseRate)) continue;
                $basePrice = $carPrice->price * $baseRate->value / 100;
                $prices = collect();
                $prices->push([
                    'name' => 'base_price',
                    'value' => round($basePrice)
                ]);
                $prices->push([
                    'name' => 'admin_fee',
                    'value' => round($insurance->admin_fee)
                ]);

                //additional here
                foreach ($additional->all() as $addition) {
                    if ($addition == 'tpl' || $addition == 'tpl_comm') {
                        $prices->push([
                            'name' => $addition,
                            'value' => round($additionalValue['tpl_value'] * $insurance[$addition] / 100)
                        ]);
                    } elseif ($addition == 'pa_driver') {
                        $prices->push([
                            'name' => $addition,
                            'value' => round($additionalValue['pa_driver_value'] * $insurance[$addition] / 100)
                        ]);
                    } elseif ($addition == 'pa_passenger') {
                        $prices->push([
                            'name' => $addition,
                            'value' => round($additionalValue['pa_passenger_value'] * $insurance[$addition] / 100 * $additionalValue['pa_passenger_seats'])
                        ]);
                    } else
                        $prices->push([
                            'name' => $addition,
                            'value' => round($carPrice->price * $insurance[$addition] / 100)
                        ]);
                }

                $tempResult = [
                    'insurance_id' => $insurance->id,
                    'insurance_name' => $insurance->name,
                    'insurance_logo_path' => $insurance->logo,
                    'insurance_benefits' => $insurance->benefits,
                    'insurance_workshops_count' => $insurance->workshops_count,
                    'prices' => $prices,
                    'total_price' => round($prices->sum('value')),
                ];

                $result->push($tempResult);
            }
            return response()->json($result);
        }
        return response()->json(['message' => 'insurance_type not found or format error!'], 400);
    }

    protected function additionalParse(Request $request)
    {
        $additional = collect();
        $tpl_value = 0;
        $pa_driver_value = 0;
        $pa_passenger_value = 0;
        $pa_passenger_seats = 0;
        $option = [];

        if ($request->has('additional')) {
            if (count($request->get('additional'))) {
                $option = $request->get('additional');
            }
        }

        foreach ($option as $i) {
            if ($i == 'eqvet' || $i == 'tsfwd' || $i == 'srcc' || $i == 'ts') {
                $additional->push($i . '_' . $request->insurance_type);
            } elseif ($i == 'tpl') {
                $tpl_value = intval($request->tpl_value);
                if ($tpl_value == null)
                    return [
                        'message' => 'tpl_value not found!',
                        'status' => 400
                    ];
                elseif ($tpl_value < 1 || $tpl_value > 10000000)
                    return [
                        'message' => 'tpl_value must be between 1 - 10000000!',
                        'status' => 400
                    ];

                $additional->push($i);
            } elseif ($i == 'pa_driver') {
                $pa_driver_value = intval($request->pa_driver_value);

                if ($pa_driver_value == null)
                    return [
                        'message' => 'pa_driver_value not found!',
                        'status' => 400
                    ];
                elseif ($pa_driver_value < 5000000 || $pa_driver_value > 25000000)
                    return [
                        'message' => 'pa_driver_value must be between 5000000 - 25000000!',
                        'status' => 400
                    ];
                else
                    $additional->push($i);
            } elseif ($i == 'pa_passenger') {
                $pa_passenger_value = intval($request->pa_passenger_value);
                $pa_passenger_seats = intval($request->pa_passenger_seats);

                if ($pa_passenger_value == null)
                    return [
                        'message' => 'pa_passenger_value not found!',
                        'status' => 400
                    ];
                elseif ($pa_passenger_value < 5000000 || $pa_passenger_value > 25000000)
                    return [
                        'message' => 'pa_passenger_value must be between 5000000 - 25000000!',
                        'status' => 400
                    ];
                elseif ($pa_passenger_seats == null)
                    return [
                        'message' => 'pa_passenger_seats not found!',
                        'status' => 400
                    ];
                else
                    $additional->push($i);
            } elseif ($i == 'atpm') {
                if ($request->insurance_type == 'comp')
                    $additional->push($i);
            } else {
                return [
                    'message' => 'additional format error!',
                    'status' => 400
                ];
            }
        }
        return [
            'status' => 200,
            'value' => [
                'tpl_value' => $tpl_value,
                'pa_driver_value' => $pa_driver_value,
                'pa_passenger_value' => $pa_passenger_value,
                'pa_passenger_seats' => $pa_passenger_seats
            ],
            'additional' => $additional
        ];
    }

    public function getWorkshops($insurance_id)
    {
        $workshops = CarInsurance::findOrFail($insurance_id)->workshops;
        return response()->json($workshops);
    }

    public function getBenefits()
    {
        $benefits = CarBenefit::all();

        return response()->json($benefits);
    }
}
