<?php

namespace App\Http\Controllers\User;

use App\PropertyInsurance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PropertyProvince;
use App\PropertyCity;
use App\PropertyBenefit;

class PropertyController extends Controller
{
    //
    public function provinceIndex()
    {
        $provinces = PropertyProvince::all();
        return response()->json($provinces);
    }

    public function cityIndex($province_id)
    {
        $cities = PropertyCity::where('property_province_id', $province_id)->orderBy('name', 'asc')->get();

        return response()->json($cities);
    }

    public function calculatePremium(Request $request)
    {
        $request->validate([
            'value' => 'required',
            'class' => 'required',
            'city' => 'required'
        ]);

        $value = $request->value;
        $class = $request->class;
        $additional = [];
        if ($request->has('additional'))
            $additional = $request->additional;

        //find city
        $propertyCity = PropertyCity::findOrFail($request->city);

        //get insurances
        $insurances = PropertyInsurance::with('benefits')->get();

        $result = collect();
        foreach ($insurances as $insurance) {
            $flexas = $value * $insurance['rate_flexas_class' . $class] / 1000;
            $prices = collect();
            $prices->push([
                'name' => 'flexas',
                'value' => round($flexas)
            ]);
            $prices->push([
                'name' => 'admin_fee',
                'value' => round($insurance->admin_fee)
            ]);

            foreach ($additional as $addition) {
                if ($addition == 'tsfwd' || $addition == 'rsmdcc' || $addition == 'other') {
                    $prices->push([
                        'name' => $addition,
                        'value' => round($value * $insurance['rate_' . $addition] / 1000)
                    ]);
                } elseif ($addition == 'eqvet') {
                    $zone = $propertyCity->zone_code;
                    $prices->push([
                        'name' => $addition,
                        'value' => round($value * $insurance['rate_eqvet_zone' . $zone] / 1000)
                    ]);
                }
            }

            $result->push([
                'insurance_id' => $insurance->id,
                'insurance_name' => $insurance->name,
                'insurance_logo_path' => $insurance->logo,
                'prices' => $prices,
                'total_price' => $prices->sum('value'),
                'insurance_benefits' => $insurance->benefits,
            ]);
        }

        return response()->json($result);
    }

    public function getBenefits()
    {
        $benefits = PropertyBenefit::all();

        return response()->json($benefits);
    }
}
