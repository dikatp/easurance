<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::all();
        return response()->json($articles);
    }

    public function random()
    {
        $articles = Article::inRandomOrder()->limit(2)->get();
        return response()->json($articles);
    }
}
