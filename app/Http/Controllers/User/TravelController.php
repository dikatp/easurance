<?php

namespace App\Http\Controllers\User;

use App\TravelPackage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TravelDestination;
use App\TravelBenefit;

class TravelController extends Controller
{
    //
    public function destinationIndex($type)
    {
        $destinations = TravelDestination::where('type', '=', $type)->get();
        return response()->json($destinations);
    }

    public function calculatePremium(Request $request)
    {
        $request->validate([
            'destination_id' => 'required',
            'package_type' => 'required',
            'duration' => 'required',
        ]);

        $destination = TravelDestination::findOrFail($request->destination_id);
        $packageType = $request->package_type;
        $duration = $request->duration;

        $packages = TravelPackage::with([
            'rates' => function ($query) use ($duration) {
                $query->where('duration_from', '<=', $duration)
                    ->where('duration_to', '>=', $duration);
            },
            'insurance.benefits'
        ])->where([
            ['type', $packageType],
            ['region', $destination->type]
        ])->get();

        $result = collect();
        foreach ($packages as $package) {
            $prices = collect();
            $prices->push([
                'name' => 'base_price',
                'value' => $package->rates->first()->price
            ]);
            $prices->push([
                'name' => 'admin_fee',
                'value' => $package->insurance->admin_fee
            ]);

            $result->push([
                'insurance_name' => $package->insurance->name,
                'insurance_logo_path' => $package->insurance->logo,
                'insurance_benefits' => $package->insurance->benefits,
                'package' => $package->name,
                'prices' => $prices,
                'total_price' => $prices->sum('value'),
                'rate_id' => $package->rates->first()->id
            ]);
        }

        return response()->json($result);
    }

    public function getBenefits()
    {
        $benefits = TravelBenefit::all();

        return response()->json($benefits);
    }
}
