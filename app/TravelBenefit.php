<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelBenefit extends Model
{
    //

    protected $fillable = ['name'];

    public function insurances()
    {
        return $this->belongsToMany('App\TravelInsurance');
    }
}
