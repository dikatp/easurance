<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarBaseRate extends Model
{
    //

    protected $fillable = [
        'type',
        'category',
        'region',
        'value',
        'car_insurance_id',
    ];

    public function insurance()
    {
        return $this->belongsTo('App\CarInsurance', 'car_insurance_id');
    }
}
