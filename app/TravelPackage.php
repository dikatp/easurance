<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelPackage extends Model
{
    //

    protected $fillable = [
        'name',
        'type',
        'region',
    ];

    public function insurance()
    {
        return $this->belongsTo('App\TravelInsurance', 'travel_insurance_id');
    }

    public function rates()
    {
        return $this->hasMany('App\TravelRate')->orderBy('duration_from');
    }
}
