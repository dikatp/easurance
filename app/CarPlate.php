<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPlate extends Model
{
    //
    protected $fillable = [
        'plate',
        'region_code',
    ];
}
