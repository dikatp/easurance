<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyBenefit extends Model
{
    //
    protected $fillable = [
        'name'
    ];

    public function insurances()
    {
        return $this->belongsToMany('App\PropertyInsurance');
    }
}
