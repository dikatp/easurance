<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarWishlist extends Model
{
    //

    protected $fillable = [
        'user_id',
        'insurance_name',
        'car_name',
        'car_year',
        'insurance_type',
        'prices',
        'total_price'
    ];

    public function setPricesAttribute($value){
        $this->attributes['prices'] = http_build_query($value);
    }
    public function getPricesAttribute(){
        $price = [];
        parse_str($this->attributes['prices'],$price);
        return $price;
    }
}
