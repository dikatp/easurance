<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyWishlist extends Model
{
    //
    protected $fillable = [
        'user_id',
        'insurance_name',
        'province',
        'city',
        'address',
        'value',
        'prices',
        'total_price'
    ];

    public function setPricesAttribute($value)
    {
        $this->attributes['prices'] = http_build_query($value);
    }
    public function getPricesAttribute()
    {
        $price = [];
        parse_str($this->attributes['prices'], $price);
        return $price;
    }
}
