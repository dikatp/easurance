<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelInsurance extends Model
{
    //

    protected $fillable = [
        'name',
        'logo',
        'admin_fee',
    ];

    public function benefits()
    {
        return $this->belongsToMany('App\TravelBenefit');
    }

    public function packages()
    {
        return $this->hasMany('App\TravelPackage');
    }
}
