<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPrice extends Model
{
    //

    protected $fillable = [
        'price',
        'year',
        'car_series_id',
    ];

    public function series()
    {
        return $this->belongsTo('App\CarSeries', 'car_series_id');
    }
}
