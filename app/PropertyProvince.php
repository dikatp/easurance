<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyProvince extends Model
{
    //

    protected $fillable = [
        'name',
    ];

    public function cities()
    {
        return $this->hasMany('App\PropertyCity');
    }
}
