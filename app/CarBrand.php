<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarBrand extends Model
{
    //

    protected $fillable = ['name'];

    public function types()
    {
        return $this->hasMany('App\CarType');
    }

    public function series()
    {
        return $this->hasManyThrough('App\CarSeries', 'App\CarType');
    }
}
