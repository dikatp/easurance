<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{
    //
    protected $fillable = [
        'name',
        'car_brand_id',
    ];

    public function brand()
    {
        return $this->belongsTo('App\CarBrand', 'car_brand_id');
    }

    public function series()
    {
        return $this->hasMany('App\CarSeries');
    }
}
