<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelRate extends Model
{
    //

    protected $fillable = [
        'duration_from',
        'duration_to',
        'price',
        'travel_package_id',
    ];

    public function package()
    {
        return $this->belongsTo('App\TravelPackage', 'travel_package_id');
    }
}
