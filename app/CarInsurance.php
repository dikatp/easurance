<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarInsurance extends Model
{
    //

    protected $fillable = [
        'name',
        'logo',
        'admin_fee',
        'eqvet_comp',
        'eqvet_tlo',
        'tsfwd_comp',
        'tsfwd_tlo',
        'srcc_comp',
        'srcc_tlo',
        'ts_comp',
        'ts_tlo',
        'tpl',
        'pa_driver',
        'pa_passenger',
        'atpm',
    ];

    public function benefits()
    {
        return $this->belongsToMany('App\CarBenefit');
    }

    public function workshops()
    {
        return $this->belongsToMany('App\CarWorkshop');
    }

    public function base_rates()
    {
        return $this->hasMany('App\CarBaseRate');
    }
}
