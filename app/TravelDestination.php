<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelDestination extends Model
{
    //
    protected $fillable = [
        'name',
        'type',
    ];
}
