<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarBenefit extends Model
{
    //

    protected $fillable = ['name'];

    public function insurances()
    {
        return $this->belongsToMany('App\CarInsurance', 'car_insurance_id');
    }
}
