<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarSeries extends Model
{
    //

    protected $fillable = [
        'name',
        'car_type_id',
    ];

    public function type()
    {
        return $this->belongsTo('App\CarType', 'car_type_id');
    }

    public function prices()
    {
        return $this->hasMany('App\CarPrice');
    }

    public function getPricesMapAttribute()
    {
        return $this->prices->mapWithKeys(function ($item) {
            return [$item['year'] => $item['price']];
        });
    }

    public function price(int $year)
    {
        return $this->hasOne('App\CarPrice')->where('year', $year)->first();
    }
}
