<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyInsurance extends Model
{
    //

    protected $fillable = [
        'name',
        'logo',
        'admin_fee',
        'rate_flexas_class1',
        'rate_flexas_class2',
        'rate_flexas_class3',
        'rate_rsmdcc',
        'rate_other',
        'rate_tsfwd',
        'rate_eqvet_zone1',
        'rate_eqvet_zone2',
        'rate_eqvet_zone3',
        'rate_eqvet_zone4',
        'rate_eqvet_zone5',
    ];

    public function benefits()
    {
        return $this->belongsToMany('App\PropertyBenefit');
    }
}
