<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarWorkshop extends Model
{
    //
    protected $fillable = [
        'name',
        'address',
        'city',
        'phone',
    ];

    public function insurances()
    {
        return $this->belongsToMany('App\CarInsurance');
    }
}
