/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import moment from 'moment';
Vue.prototype.$moment = moment;

Vue.prototype.$currency = new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' });

import swal from 'sweetalert2';
Vue.prototype.$swal = swal;
Vue.prototype.$alert = swal.mixin({
    //toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 5000
});

import VueRouter from 'vue-router'
Vue.use(VueRouter);

let routes = [
    { path: '/', component: require('./components/pages/Home.vue').default },
    //car insurance
    { path: '/car-insurance', component: require('./components/pages/CarInsurance.vue').default, },
    { path: '/car-insurance/create', component: require('./components/forms/CarInsuranceForm.vue').default },
    { path: '/car-insurance/:id', component: require('./components/pages/CarInsuranceDetail.vue').default },
    { path: '/car-insurance/:id/edit', component: require('./components/forms/CarInsuranceForm.vue').default, props: { isEdit: true } },
    { path: '/car-insurance-benefit', component: require('./components/forms/CarInsuranceBenefit.vue').default },
    { path: '/car-insurance-workshop', component: require('./components/forms/CarInsuranceWorkshop.vue').default },

    { path: '/cars', component: require('./components/pages/Car.vue').default },

    { path: '/car-benefit', component: require('./components/pages/CarBenefit.vue').default, },
    { path: '/car-benefit/create', component: require('./components/forms/CarBenefitForm.vue').default, },
    { path: '/car-benefit/:id/edit', component: require('./components/forms/CarBenefitForm.vue').default, props: { isEdit: true } },

    { path: '/car-workshop', component: require('./components/pages/CarWorkshop.vue').default, },
    { path: '/car-workshop/create', component: require('./components/forms/CarWorkshopForm.vue').default },
    { path: '/car-workshop/:id/edit', component: require('./components/forms/CarWorkshopForm.vue').default, props: { isEdit: true } },

    { path: '/car-plate', component: require('./components/pages/CarPlate.vue').default, },
    { path: '/car-plate/create', component: require('./components/forms/CarPlateForm.vue').default, },
    { path: '/car-plate/:id/edit', component: require('./components/forms/CarPlateForm.vue').default, props: { isEdit: true } },

    //property insurance
    { path: '/property-insurance', component: require('./components/pages/PropertyInsurance.vue').default, },
    { path: '/property-insurance/create', component: require('./components/forms/PropertyInsuranceForm.vue').default, },
    { path: '/property-insurance/:id', component: require('./components/pages/PropertyInsuranceDetail.vue').default },
    { path: '/property-insurance/:id/edit', component: require('./components/forms/PropertyInsuranceForm.vue').default, props: { isEdit: true } },
    { path: '/property-insurance-benefit', component: require('./components/forms/PropertyInsuranceBenefit.vue').default },

    { path: '/property-benefit', component: require('./components/pages/PropertyBenefit.vue').default, },
    { path: '/property-benefit/create', component: require('./components/forms/PropertyBenefitForm.vue').default, },
    { path: '/property-benefit/:id/edit', component: require('./components/forms/PropertyBenefitForm.vue').default, props: { isEdit: true } },

    { path: '/property-region', component: require('./components/pages/PropertyRegion.vue').default, },
    { path: '/property-region/create', component: require('./components/forms/PropertyRegionForm.vue').default, },
    { path: '/property-region/:id', component: require('./components/pages/PropertyRegionDetail.vue').default },
    { path: '/property-region/:id/edit', component: require('./components/forms/PropertyRegionForm.vue').default, props: { isEdit: true } },

    //travel insurance
    { path: '/travel-insurance', component: require('./components/pages/TravelInsurance.vue').default, },
    { path: '/travel-insurance/create', component: require('./components/forms/TravelInsuranceForm.vue').default, },
    { path: '/travel-insurance/:id', component: require('./components/pages/TravelInsuranceDetail.vue').default },
    { path: '/travel-insurance/:id/edit', component: require('./components/forms/TravelInsuranceForm.vue').default, props: { isEdit: true } },
    { path: '/travel-insurance-benefit', component: require('./components/forms/TravelInsuranceBenefit.vue').default },

    { path: '/travel-benefit', component: require('./components/pages/TravelBenefit.vue').default, },
    { path: '/travel-benefit/create', component: require('./components/forms/TravelBenefitForm.vue').default, },
    { path: '/travel-benefit/:id/edit', component: require('./components/forms/TravelBenefitForm.vue').default, props: { isEdit: true } },

    { path: '/travel-destination', component: require('./components/pages/TravelDestination.vue').default, },
    { path: '/travel-destination/create', component: require('./components/forms/TravelDestinationForm.vue').default, },
    { path: '/travel-destination/:id/edit', component: require('./components/forms/TravelDestinationForm.vue').default, props: { isEdit: true } },

    //article
    { path: '/article', component: require('./components/pages/Article.vue').default, },
    { path: '/article/create', component: require('./components/forms/ArticleForm.vue').default, },
    { path: '/article/:id', component: require('./components/pages/ArticleDetail.vue').default },
    { path: '/article/:id/edit', component: require('./components/forms/ArticleForm.vue').default, props: { isEdit: true } },

    //faq
    { path: '/faq', component: require('./components/pages/Faq.vue').default, },
    { path: '/faq/create', component: require('./components/forms/FaqForm.vue').default, },
    { path: '/faq/:id/edit', component: require('./components/forms/FaqForm.vue').default, props: { isEdit: true } },
];
const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('header-nav', require('./components/layouts/HeaderNav.vue').default);
Vue.component('side-nav', require('./components/layouts/SideNav.vue').default);
Vue.component('dynamic-table', require('./components/layouts/DynamicTable.vue').default);
Vue.component('edit-text', require('./components/layouts/EditText.vue').default);

Vue.component('car-brand', require('./components/module/CarBrand.vue').default);
Vue.component('car-type', require('./components/module/CarType.vue').default);
Vue.component('car-series', require('./components/module/CarSeries.vue').default);
Vue.component('car-price', require('./components/module/CarPrice.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
