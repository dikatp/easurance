<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Easurance Admin</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="/js/app.js" defer></script>

    <link rel="stylesheet" href="/css/app.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,600" rel="stylesheet" type="text/css">

</head>
<body>
    <div id="app" class="app">
        <header-nav :user="{{json_encode(Auth::user())}}"></header-nav>
        <div class="body-container">
            <side-nav></side-nav>
            <div class="main-content">
                <router-view></router-view>
            </div>
        </div>
    </div>
</body>
</html>
