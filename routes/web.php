<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('site-api/admin')->middleware('auth:admin')->group(function () {
    //Admin Car Insurances
    Route::get('/car-insurances', 'Admin\CarInsuranceController@insuranceIndex');
    Route::get('/car-insurances/{id}', 'Admin\CarInsuranceController@insuranceShow');
    Route::post('/car-insurances', 'Admin\CarInsuranceController@insuranceStore');
    Route::post('/car-insurances/{id}', 'Admin\CarInsuranceController@insuranceUpdate');
    Route::delete('/car-insurances/{id}/delete', 'Admin\CarInsuranceController@insuranceDestroy');
    Route::get('/car-insurances/{id}/benefits', 'Admin\CarInsuranceController@insuranceBenefitIndex');
    Route::post('/car-insurances/{id}/benefits', 'Admin\CarInsuranceController@insuranceBenefitSync');
    Route::get('/car-insurances/{id}/workshops', 'Admin\CarInsuranceController@insuranceWorkshopIndex');
    Route::post('/car-insurances/{id}/workshops', 'Admin\CarInsuranceController@insuranceWorkshopSync');



    //Admin Car Additional Rates
    //    Route::get('/car-insurances/{insurance_id}/additional-rates', 'Admin\CarInsuranceController@addRateIndex');
    //    Route::get('/car-insurances/{insurance_id}/additional-rates/{id}', 'Admin\CarInsuranceController@addRateShow');
    //    Route::post('/car-insurances/{insurance_id}/additional-rates', 'Admin\CarInsuranceController@addRateStore');
    //    Route::post('/car-insurances/{insurance_id}/additional-rates/{id}', 'Admin\CarInsuranceController@addRateUpdate');
    //    Route::delete('/car-insurances/{insurance_id}/additional-rates/{id}/delete', 'Admin\CarInsuranceController@addRateDestroy');
    //
    //Admin Car Base Rates
    //    Route::get('/car-insurances/{insurance_id}/base-rates', 'Admin\CarInsuranceController@baseRateIndex');
    //    Route::get('/car-insurances/{insurance_id}/base-rates/{id}', 'Admin\CarInsuranceController@baseRateShow');
    //    Route::post('/car-insurances/{insurance_id}/base-rates', 'Admin\CarInsuranceController@baseRateStore');
    //    Route::post('/car-insurances/{insurance_id}/base-rates/{id}', 'Admin\CarInsuranceController@baseRateUpdate');
    //    Route::delete('/car-insurances/{insurance_id}/base-rates/{id}/delete', 'Admin\CarInsuranceController@baseRateDestroy');

    //Admin Car Benefits
    Route::get('/car-benefits', 'Admin\CarBenefitController@index');
    Route::get('/car-benefits/{id}', 'Admin\CarBenefitController@show');
    Route::post('/car-benefits', 'Admin\CarBenefitController@store');
    Route::post('/car-benefits/{id}', 'Admin\CarBenefitController@update');
    Route::delete('/car-benefits/{id}/delete', 'Admin\CarBenefitController@destroy');

    //Admin Car Workshops
    Route::get('/car-workshops', 'Admin\CarWorkshopController@index');
    Route::get('/car-workshops/{id}', 'Admin\CarWorkshopController@show');
    Route::post('/car-workshops', 'Admin\CarWorkshopController@store');
    Route::post('/car-workshops/{id}', 'Admin\CarWorkshopController@update');
    Route::delete('/car-workshops/{id}/delete', 'Admin\CarWorkshopController@destroy');

    //Admin Car Plates
    Route::get('/plates', 'Admin\CarPlateController@index');
    Route::get('/plates/{id}', 'Admin\CarPlateController@show');
    Route::post('/plates', 'Admin\CarPlateController@store');
    Route::post('/plates/{id}', 'Admin\CarPlateController@update');
    Route::delete('/plates/{id}/delete', 'Admin\CarPlateController@destroy');

    //Admin Car Brands
    Route::get('/cars/brands', 'Admin\CarController@brandIndex');
    Route::get('/cars/brands/{id}', 'Admin\CarController@brandShow');
    Route::post('/cars/brands', 'Admin\CarController@brandStore');
    Route::post('/cars/brands/{id}', 'Admin\CarController@brandUpdate');
    Route::delete('/cars/brands/{id}', 'Admin\CarController@brandDestroy');

    //Admin Car Types
    Route::get('/cars/brands/{brand_id}/types', 'Admin\CarController@typeIndex');
    Route::get('/cars/types/{id}', 'Admin\CarController@typeShow');
    Route::post('/cars/brands/{brand_id}/types', 'Admin\CarController@typeStore');
    Route::post('/cars/types/{id}', 'Admin\CarController@typeUpdate');
    Route::delete('/cars/types/{id}', 'Admin\CarController@typeDestroy');

    //Admin Car Series
    Route::get('/cars/types/{type_id}/series', 'Admin\CarController@seriesIndex');
    Route::get('/cars/series/{id}', 'Admin\CarController@seriesShow');
    Route::post('/cars/types/{type_id}/series', 'Admin\CarController@seriesStore');
    Route::post('/cars/series/{id}', 'Admin\CarController@seriesUpdate');
    Route::delete('/cars/series/{id}', 'Admin\CarController@seriesDestroy');

    //Admin Car Prices
    Route::get('/cars/series/{series_id}/prices', 'Admin\CarController@priceIndex');
    Route::get('/cars/prices/{id}', 'Admin\CarController@priceShow');
    Route::post('/cars/series/{series_id}/prices', 'Admin\CarController@priceStore');
    Route::delete('/cars/series/{series_id}/prices', 'Admin\CarController@seriesPriceDestroy');
    Route::post('/cars/prices/{id}', 'Admin\CarController@priceUpdate');
    Route::delete('/cars/prices/{id}/delete', 'Admin\CarController@priceDestroy');

    //Admin Property Insurances
    Route::get('/property-insurances', 'Admin\PropertyInsuranceController@insuranceIndex');
    Route::get('/property-insurances/{id}', 'Admin\PropertyInsuranceController@insuranceShow');
    Route::post('/property-insurances', 'Admin\PropertyInsuranceController@insuranceStore');
    Route::post('/property-insurances/{id}', 'Admin\PropertyInsuranceController@insuranceUpdate');
    Route::delete('/property-insurances/{id}/delete', 'Admin\PropertyInsuranceController@insuranceDestroy');
    Route::get('/property-insurances/{id}/benefits', 'Admin\PropertyInsuranceController@insuranceBenefitIndex');
    Route::post('/property-insurances/{id}/benefits', 'Admin\PropertyInsuranceController@insuranceBenefitSync');

    //Admin Property Benefits
    Route::get('/property-benefits', 'Admin\PropertyBenefitController@index');
    Route::get('/property-benefits/{id}', 'Admin\PropertyBenefitController@show');
    Route::post('/property-benefits', 'Admin\PropertyBenefitController@store');
    Route::post('/property-benefits/{id}', 'Admin\PropertyBenefitController@update');
    Route::delete('/property-benefits/{id}/delete', 'Admin\PropertyBenefitController@destroy');

    //Admin Property Provinces
    Route::get('/properties/provinces', 'Admin\PropertyController@provinceIndex');
    Route::get('/properties/provinces/{id}', 'Admin\PropertyController@provinceShow');
    Route::post('/properties/provinces', 'Admin\PropertyController@provinceStore');
    Route::post('/properties/provinces/{id}', 'Admin\PropertyController@provinceUpdate');
    Route::delete('/properties/provinces/{id}/delete', 'Admin\PropertyController@provinceDestroy');

    //Admin Property Cities
    Route::get('/properties/provinces/{province_id}/cities', 'Admin\PropertyController@cityIndex');
    Route::get('/properties/provinces/{province_id}/cities/{id}', 'Admin\PropertyController@cityShow');
    Route::post('/properties/provinces/{province_id}/cities', 'Admin\PropertyController@cityStore');
    Route::post('/properties/provinces/{province_id}/cities/{id}', 'Admin\PropertyController@cityUpdate');
    Route::delete('/properties/provinces/{province_id}/cities/{id}/delete', 'Admin\PropertyController@cityDestroy');

    //Admin Travel Insurances
    Route::get('/travel-insurances', 'Admin\TravelInsuranceController@insuranceIndex');
    Route::get('/travel-insurances/{id}', 'Admin\TravelInsuranceController@insuranceShow');
    Route::post('/travel-insurances', 'Admin\TravelInsuranceController@insuranceStore');
    Route::post('/travel-insurances/{id}', 'Admin\TravelInsuranceController@insuranceUpdate');
    Route::delete('/travel-insurances/{id}/delete', 'Admin\TravelInsuranceController@insuranceDestroy');
    Route::get('/travel-insurances/{id}/benefits', 'Admin\TravelInsuranceController@insuranceBenefitIndex');
    Route::post('/travel-insurances/{id}/benefits', 'Admin\TravelInsuranceController@insuranceBenefitSync');

    //Admin Travel Packages
    Route::get('/travel-insurances/{insurance_id}/packages', 'Admin\TravelInsuranceController@packageIndex');
    Route::get('/travel-insurances/{insurance_id}/packages/{id}', 'Admin\TravelInsuranceController@packageShow');
    Route::post('/travel-insurances/{insurance_id}/packages', 'Admin\TravelInsuranceController@packageStore');
    Route::post('/travel-insurances/{insurance_id}/packages/{id}', 'Admin\TravelInsuranceController@packageUpdate');
    Route::delete('/travel-insurances/{insurance_id}/packages/{id}/delete', 'Admin\TravelInsuranceController@packageDestroy');

    //Admin Travel Rates
    Route::get('/travel-insurances/packages/{package_id}/rates', 'Admin\TravelInsuranceController@rateIndex');
    Route::get('/travel-insurances/packages/{package_id}/rates/{id}', 'Admin\TravelInsuranceController@rateShow');
    Route::post('/travel-insurances/packages/{package_id}/rates', 'Admin\TravelInsuranceController@rateStore');
    Route::post('/travel-insurances/packages/{package_id}/rates/{id}', 'Admin\TravelInsuranceController@rateUpdate');
    Route::delete('/travel-insurances/packages/{package_id}/rates/{id}/delete', 'Admin\TravelInsuranceController@rateDestroy');

    //Admin Travel Benefits
    Route::get('/travel-benefits', 'Admin\TravelBenefitController@index');
    Route::get('/travel-benefits/{id}', 'Admin\TravelBenefitController@show');
    Route::post('/travel-benefits', 'Admin\TravelBenefitController@store');
    Route::post('/travel-benefits/{id}', 'Admin\TravelBenefitController@update');
    Route::delete('/travel-benefits/{id}/delete', 'Admin\TravelBenefitController@destroy');

    //Admin Travel Destinations
    Route::get('travels/destinations', 'Admin\TravelController@index');
    Route::get('travels/destinations/{id}', 'Admin\TravelController@show');
    Route::post('travels/destinations', 'Admin\TravelController@store');
    Route::post('travels/destinations/{id}', 'Admin\TravelController@update');
    Route::delete('travels/destinations/{id}/delete', 'Admin\TravelController@destroy');

    //Admin Articles
    Route::get('articles', 'Admin\ArticleController@index');
    Route::get('articles/{id}', 'Admin\ArticleController@show');
    Route::post('articles', 'Admin\ArticleController@store');
    Route::post('articles/{id}', 'Admin\ArticleController@update');
    Route::delete('articles/{id}/delete', 'Admin\ArticleController@destroy');

    //Admin Faqs
    Route::get('faqs', 'Admin\FaqController@index');
    Route::get('faqs/{id}', 'Admin\FaqController@show');
    Route::post('faqs', 'Admin\FaqController@store');
    Route::post('faqs/{id}', 'Admin\FaqController@update');
    Route::delete('faqs/{id}/delete', 'Admin\FaqController@destroy');
});

Route::get(
    '/login',
    'Auth\AdminAuthController@showLoginForm'
)->name('login');
Route::post('/login', 'Auth\AdminAuthController@login')->name('admin.login.submit');
Route::get('logout/', 'Auth\AdminAuthController@logout')->name('admin.logout');
Route::middleware('auth:admin')->group(function () {
    Route::get('/', function () {
        return view('main');
    })->name('admin');
    Route::get('{path}', function () {
        return view('main');
    })->where('path', '[A-z\d-\/_.]+?');
});
