<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
], function ($router) {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('register', 'AuthController@register');
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    });

    Route::get('/wishlist/car', 'User\WishlistController@getCarWishlist');
    Route::get('/wishlist/property', 'User\WishlistController@getPropertyWishlist');
    Route::get('/wishlist/travel', 'User\WishlistController@getTravelWishlist');

    Route::post('/wishlist/car', 'User\WishlistController@postCarWishlist');
    Route::post('/wishlist/property', 'User\WishlistController@postPropertyWishlist');
    Route::post('/wishlist/travel', 'User\WishlistController@postTravelWishlist');

    Route::delete('/wishlist/car/{id}', 'User\WishlistController@destroyCarWishlist');
    Route::delete('/wishlist/property/{id}', 'User\WishlistController@destroyPropertyWishlist');
    Route::delete('/wishlist/travel/{id}', 'User\WishlistController@destroyTravelWishlist');
});

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('/property/provinces', 'User\PropertyController@provinceIndex');
Route::get('/property/provinces/{province_id}/cities', 'User\PropertyController@cityIndex');
Route::post('/property/calculate-premium', 'User\PropertyController@calculatePremium');
Route::get('/property/benefits', 'User\PropertyController@getBenefits');

Route::get('/car/brands/{year}', 'User\CarController@brandIndex');
Route::get('/car/brands/{brand_id}/types', 'User\CarController@typeIndex');
Route::get('/car/types/{type_id}/series', 'User\CarController@seriesIndex');
Route::get('/car/plates', 'User\CarController@plateIndex');
Route::post('/car/calculate-premium', 'User\CarController@calculatePremium');
Route::get('/car-insurance/{insurance_id}/workshops', 'User\CarController@getWorkshops');
Route::get('/car/benefits', 'User\CarController@getBenefits');

Route::get('/travel/destinations/{type}', 'User\TravelController@destinationIndex');
Route::post('/travel/calculate-premium', 'User\TravelController@calculatePremium');
Route::get('/travel/benefits', 'User\TravelController@getBenefits');

Route::get('/articles', 'User\ArticleController@index');
Route::get('/articles/random', 'User\ArticleController@random');

Route::get('/faqs', 'User\FaqController@index');
