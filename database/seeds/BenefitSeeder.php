<?php

use Illuminate\Database\Seeder;

class BenefitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Benefit Mobil
        DB::table('car_benefits')->insert([
            [
                'name' => 'Layanan derek gratis',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Mobil pengganti',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Emergency road assistance',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Tanpa survey',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Akomodasi biaya sewa kendaraan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Klaim langsung dimanapun',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Antar jemput kendaraan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Pertanggungan non standart accessories',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya ambulan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Santunan biaya klaim',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya taksi',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya taksi online',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Pertanggungan barang pribadi',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Layanan perpanjangan stnk',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => '24/7 emergency services',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Garansi bengkel 3 bulan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya ambulan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);

        //Benefit Properti
        DB::table('property_benefits')->insert([
            [
                'name' => 'Akomodasi tempat tinggal sementara',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya jasa profesional untuk perbaikan rumah',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Penambahan otomatis untuk barang baru',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya untuk PRT, Satpam, dll',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Hotline 24 jam',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya perabotan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya pengobatan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kerusakan untuk mencegah kebakaran',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya kunci dan anak kunci',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Santunan kematian',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Pertanggungan tertabrak kendaraan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);

        //Benefit Perjalanan
        DB::table('travel_benefits')->insert([
            [
                'name' => 'Maksimal hingga 80 tahun',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Tidak ada maksimal umur',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya medis lanjutan di negara asal dan perawatan gigi',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Penggantian biaya pemakaian telepon dan internet darurat',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Penggantian biaya liburan yang hilang',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Penggantian biaya sewa kendaraan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya orang yang mendampingi ketika dirawat',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya kerusakan atau kehilangan peralatan golf',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Penggantian kehilangan uang',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Pertanggungan kecelakaan saat menuju bandara',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kerugian akibat pencurian rumah saat melakukan perjalanan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kerugian akibat kebakaran rumah saat melakukan perjalanan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya akomodasi untuk kunjungan keluarga ketika dirawat',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kehilangan laptop',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Pertanggungan terhadap serangan terorisme',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Biaya pengurusan dokumen yang hilang',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Pertanggungan jika tidak dapat melakukan perjalanan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Santunan kematian',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Santunan kecacatan',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);
    }
}
