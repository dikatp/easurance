<?php

use Illuminate\Database\Seeder;

class SeriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_series')->insert([
            [
                "name" => "1.5 M/T",
                "car_type_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 A/T ",
                "car_type_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "2.4 M/T",
                "car_type_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " 2.4 A/T ",
                "car_type_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "2.0 FWD A/T ",
                "car_type_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "2.0 LTZ AT FWD ",
                "car_type_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 LT M/T ",
                "car_type_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 LS M/T ",
                "car_type_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 LTZ M/T ",
                "car_type_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 LTZ A/T",
                "car_type_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.4 PREMIER A/T",
                "car_type_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "PANCA T ACTIVE",
                "car_type_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " PANCA T M/T",
                "car_type_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " PANCA T OPT",
                "car_type_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " 2.3L XLT A/T ",
                "car_type_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " 2.3L XLS A/T ",
                "car_type_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " 2.5 L TDI 4X2 - XLT A/T ",
                "car_type_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "2.5 L TDCI 4X2 - XLT M/T",
                "car_type_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " 2.5 L TDCI 4X2 - XLT A/T ",
                "car_type_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " 1.4 STYLE M/T ",
                "car_type_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.6 SPORT M/T",
                "car_type_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.6 SPORT A/T ",
                "car_type_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "E M/T ",
                "car_type_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "E A/T",
                "car_type_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "RS M/T ",
                "car_type_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "RS A/T ",
                "car_type_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 A M/T",
                "car_type_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 E A/T (Cvt) ",
                "car_type_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.8 E CVT ",
                "car_type_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "E M/T ",
                "car_type_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "E A/T ",
                "car_type_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "RS M/T ",
                "car_type_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "RS A/T ",
                "car_type_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "E MT ",
                "car_type_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "E A/T ",
                "car_type_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "G MT ",
                "car_type_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "G A/T ",
                "car_type_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.3 E A/T",
                "car_type_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.3 G M/T ",
                "car_type_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.3 G A/T",
                "car_type_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 G M/T ",
                "car_type_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "2.4 V A/T",
                "car_type_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " 2.5 V A/T",
                "car_type_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "2.5 HYBRID A/T ",
                "car_type_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 E M/T ",
                "car_type_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 E A/T",
                "car_type_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 S TRD SPORTIVO M/T ",
                "car_type_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "1.5 S TRD SPORTIVO A/T ",
                "car_type_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "GLX M/T",
                "car_type_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " EXCEED M/T ",
                "car_type_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "EXCEED A/T ",
                "car_type_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " SPORT M/T ",
                "car_type_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => " SPORT A/T ",
                "car_type_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "ULTIMATE A/T ",
                "car_type_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "GLX M/T ",
                "car_type_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "GLS CVT",
                "car_type_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "EXCEED CVT ",
                "car_type_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "EXCEED 4x2 A/T",
                "car_type_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "EXCEED 4x4 A/T ",
                "car_type_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "GLX 4x4 M/T ",
                "car_type_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "name" => "LIMITED DAKAR 4X2 ",
                "car_type_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);
    }
}
