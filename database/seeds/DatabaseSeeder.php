<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(DummySeeder::class);
        $this->call(BenefitSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(DestinationSeeder::class);
        $this->call(PlateSeeder::class);
        $this->call(ProvinceSeeder::class);
        $this->call(WorkshopSeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(TypeSeeder::class);
        $this->call(SeriesSeeder::class);
        $this->call(PriceSeeder::class);
        $this->call(TravelInsuranceSeeder::class);
        $this->call(CarInsuranceSeeder::class);
        $this->call(PropertyInsuranceSeeder::class);
    }
}
