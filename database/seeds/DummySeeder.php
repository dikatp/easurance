<?php

use Illuminate\Database\Seeder;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('articles')->insert([
            [
                'title' => 'Daftar Perusahaan Asuransi Terbesar di Indonesia',
                'thumbnail' => '/media/image/article/article1.png',
                'content' => '<p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Masyarakat Indonesia di
                era modern seperti sekarang ini sudah sadar akan pentingnya asuransi. Tidak
                hanya dari asuransi resmi pemerintah saja, akan tetapi peran dukungan dari
                asuransi swasta juga sangat penting. Namun Anda mungkin masih kebingungan
                memilih seperti apa produk asuransi paling baik bahkan sekarang sudah terdapat
                beberapa daftar perusahaan asuransi terbesar di Indonesia.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Tidak hanya menghadirkan
                bentuk pelayanan terbaik saja, akan tetapi dari sebuah perusahaan asuransi
                terbesar pastinya ada banyak keuntungan yang bisa dirasakan oleh setiap
                nasabahnya. Langsung saja kita cek seperti apa perusahaan asuransi paling besar
                di Indonesia sampai sekarang ini.</span><span lang="EN-ID" style="font-size:12.0pt;
                font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
                mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><b><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-language:EN-ID">AIA
                Financial<o:p></o:p></span></b></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Pertama Anda bisa memilih
                asuransi terbesar di Indonesia yakni AIA Financial dimana menjadi salah satu
                pilihan terbaik. Bahkan perusahaan asuransi jiwa terkemuka di Indonesia ini
                sudah diawasi langsung oleh Otoritas Jasa Keuangan. AIA menjadi anak perusahaan
                AIA Group kemudian perubahan nama terjadi pada tahun 2009 dimana dahulu bernama
                PT AIG Life kini menjadi PT AIA Financial.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:
                &quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><b><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-language:EN-ID">AXA
                Mandiri<o:p></o:p></span></b></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Kedua ada AXA Mandiri
                dimana sudah berdiri sejak tahun 2004 kemudian terus berkembang menjadi bisnis
                asuransi umum di tahun 2011. Menariknya pelayanan AXA Mandiri ini semakin
                membaik dan lengkap bahkan terus menguat hingga dapat fokus menyediakan beragam
                solusi kebutuhan nasabahnya baik dari sisi investasi sampai perlindungan jiwa
                dan aset bahkan layanan keuangan.</span><span lang="EN-ID" style="font-size:12.0pt;
                font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
                mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><b><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-language:EN-ID">Allianz<o:p></o:p></span></b></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Pada akses ketiga sudah
                menghadirkan pilihan terbaik bahkan sudah dimulai pada tahun 1981. Bahkan PT
                Asuransi Allianz Utama Indonesia menjadi perusahaan asuransi umum sejak tahun
                1989 kemudian memberikan bisnis asuransi jiwa, dana pensiun, dan kesehatan dengan
                mendirikan PT Asuransi Allianz Life Indonesia di tahun 1996. Kini sudah lebih
                dari 1400 karyawan kemudian terdapat lebih dari 20 ribu tenaga penjualan dan
                100 kantor pemasaran di 53 kota. Bahkan sudah lebih melayani ratusan ribu
                nasabah.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><b><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-language:EN-ID">Prudential<o:p></o:p></span></b></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Salah satu asuransi
                terbesar di Indonesia yakni Prudential hingga sekarang juga menghadirkan bentuk
                pelayanan tebraik bahkan sudah didirikan tahun 1995. Hingga kini Prudential
                sanggup menghadirkan sebuah perusahaan asuransi berkualitas terbaik bahkan jumlah
                nasabahnya sendiri terus bertambah. Sejak tahun 1999 Purdential sudah menjadi
                salah satu pemimpin asuransi terbaik dengan kategori produk terlengkap di
                Indonesia.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><b><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-language:EN-ID">Manulife<o:p></o:p></span></b></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">PT ASuransi Jiwa Manulife
                Indonesia menghadirkan banyak layanan kesehatan, keuangan, dan masih banyak
                lainnya. Manulife Indonesia sudah dikenal memberikan manfaat terbaiknya seperti
                menawarkan layanan asuransi kesehatan, kecelakaan, hingga dana pensiun dan
                layanan investasi. Oleh sebab itu sekarang ini banyak nasabah mengandalkan
                Manulife hingga memperlihatkan banyak solusi terbaik bagi investasi ataupun
                kebutuhan asuransi.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:
                &quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:
                EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><b><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-language:EN-ID">Generali<o:p></o:p></span></b></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Salah satu perusahaan
                asuransi ternama bahkan menjadi pilihan banyak masyarakat sebagai asuransi
                terbesar di Indonesia yakni Generali. Ternyata Generali sudah didirikan sejak
                tahun 1831 kemudian masuk ke Indonesia dengan menawarkan asuransi jiwa hingga
                investasi. Bahkan sekarang sudah lebih dari 11 ribu agen pemasaran hingga kantor
                agen di seluruh Indonesia. Maka dari itu tak heran bila Generali sebagai
                asuransi paling besar di Indonesia.</span><span lang="EN-ID" style="font-size:
                12.0pt;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
                mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Setiap perusahaan&nbsp;</span><span lang="EN-ID"><a href="https://www.disitu.com/Asuransi/Asuransi-Mobil"><span style="font-size:
                12.0pt;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
                color:black;mso-color-alt:windowtext;mso-fareast-language:EN-ID">asuransi</span></a></span><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;di Indonesia masih menghadirkan banyak keuntungan
                di dalamnya. Sehingga setiap nasabah harus bisa memperhatikan banyak solusi
                penting sampai akhirnya mampu menghadirkan strategi bisnis investasi sekaligus
                memberikan banyak solusi cepat untuk memberi proteksi baik dalam bentuk
                asuransi kesehatan, jiwa, hingga beberapa produk terbaik dengan tujuan
                memberikan manfaat penting bagi siapa saja yang saat ini membutuhkan asuransi
                terbesar di Indonesia.</span></p>',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'title' => 'Pengertian Asuransi Secara Umum Bagi Masyarakat',
                'thumbnail' => '/media/image/article/article2.png',
                'content' => '<p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Kesehatan seseorang bisa saja mengalami gangguan
                hingga butuh perawatan ekstra hingga akhirnya membuat seseorang butuh layanan
                asuransi. Bahkan hingga kini pihak pemerintah dan swasta masih menghadirkan
                produk asuransi kesehatan hingga asuransi kecelakaan,<a href="https://www.disitu.com/Asuransi/Asuransi-Mobil"><span style="color:black;
                mso-color-alt:windowtext">asuransi kendaraan</span></a>&nbsp;dan jiwa lebih
                mendetail sampai akhirnya nasabahnya benar-benar merasa diuntungkan. Akan
                tetapi apakah Anda sudah mengerti akan pengertian asuransi secara umum?</span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Meskipun sudah banyak produk hingga label merk
                asuransi swasta yang beredar di masyarakat, tetapi ada berbagai kalangan masih
                belum mengerti seperti apa pengertian asuransi. Meskipun sudah mendapatkan
                penawaran terbaik dari semua produknya, tetap saja asuransi secara umum sampai
                manfaatnya harus dipahami betul bagi nasabahnya. Lalu seperti apa pengertian
                asuransi yang selama ini kita kenal?</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size:12.0pt;font-family:
                &quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:
                EN-ID"><o:p>&nbsp;</o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><strong><span lang="EN-ID" style="font-size: 12pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Pengertian Asuransi</span></strong><strong><span lang="EN-ID" style="font-size: 12pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><o:p></o:p></span></strong></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><strong><span lang="EN-ID" style="font-size: 12pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><o:p>&nbsp;</o:p></span></strong></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Asuransi sendiri menjadi satu mekanisme pemindahan
                resiko kepada pihak lainnya dimana sanggup memberikan jaminan kompensasi
                finansial secara utuh ataupun parsial. Tujuannya sudah jelas yakni memberikan
                pertanggungan dari kerugian sampai kerusakan ataupun sebuah gangguan akibat
                peristiwa di luar kendali tertanggung ataupun nasabah produk asuransi.</span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Nantinya dalam proses pengajuan kontrak asuransi dari
                pihak perusahaan asuransi akan menghadirkan solusi berupa ganti rugi. Tentu
                saja tujuan utama ganti rugi tersebut diberikan kepada pihak tertanggung yang
                besaran jumlah biaya penanggungan tersebut bisa berbeda sesuai dengan besaran
                premi yang dibayarkan. Nantinya pada jenis asuransi umum akan menghadirkan
                proporsional ataupun takaran tepat mengenai kerugian yang diderita oleh
                nasabah. Beberapa jenis asuransi juga memberikan perlindungan kerugian lebih
                maksimal, hingga memberikan jaminan kelangsungan bisnis, pelanggan, hingga
                pangsa pasar, hingga lainnya.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">&nbsp;</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><strong><span lang="EN-ID">Asuransi Dalam Undang-Undang No 2 Th 1992</span></strong></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><strong><span lang="EN-ID"><br></span></strong><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Masih ada pengertian asuransi secara umum dari sisi
                undang-undang no 2 tahun 1992 dimana asuransi sendiri berarti sebuah perjanjian
                antara dua pihak bahkan lebih. Nantinya pihak penanggung akan mengikatkan diri
                kepada pihak tertanggung ditambah lagi akan menerima premi asuransi yang
                nantinya diberikan sebagai penggantian kepada pihak tertanggung atas kerugian
                ataupun kehilangan keuntungan.</span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Badan yang secara khusus menyalurkan resiko disebut
                penanggung hingga nantinya perjanjian dua pihak akan dijalankan secara optimal.
                Bisa dikatakan bahwa ada beberapa proses yang harus ada di dalam asuransi mulai
                dari penentuan penanggung kemudian memberikan klaim di masa depan ditambah lagi
                ada beban biaya administrasi hingga keuntungan.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">&nbsp;</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><strong><span lang="EN-ID">Asuransi Kitab Undang-Undang Hukum Dagang(KUHD)</span></strong></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><strong><span lang="EN-ID"><br></span></strong><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Berikutnya masih ada pengertian asuransi secara umum
                berdasarkan kitab Undang-Undang Hukum Dagang atau KUHD. Menariknya dari
                definisi asuransi KUHD sendiri menjadi satu pertanggungan berdasarkan
                perjanjian dengan banyak orang ataupun seorang penanggung ditambah bersifat
                mengikat diri kepada pihak tertanggung. Nantinya aka nada penerimaan sebuah
                premi yang menjadi jaminan pergantian karena sebuah kerugian, kehilangan
                keuntungan, sampai adanya kerusakan. Tentu saja kerusakan, kehilangan, sampai
                pengurangan keuntungan tersebut bisa diderita karena peristiwa suatu hal hingga
                akhirnya bisa dijadikan jaminan kepada pihak nasabah.</span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Semua aspek penting yang ada di dalam asuransi
                menjadi kebijakan yang harus ditaati oleh kedua pihak baik penanggung dan
                tertanggung. Dalam pengertian asuransi secara umum saat ini sudah sangat meluas
                baik dari penggantian finansial dalam bentuk properti, jiwa, dan masih banyak
                lainnya. Sedangkan kejadian yang sering terjadi dengan potensi buruk terjadi
                karena kematian, kecelakaan, ataupun bencana alam, hingga kerusuhan dan
                bersifat force majeur.</span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">
                </p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Sebagai solusi ganti atas manfaat yang bisa diterima
                oleh nasabah asuransi maka dari pihak peserta asuransi tersebut harus melakukan
                pembayaran premi secara teratur sesuai dengan jangka waktu yang sudah
                ditentukan. Hingga akhirnya mendapatkan haknya berupa perlindungan sampai ganti
                rugi.</span><span lang="EN-ID"><o:p></o:p></span></p>',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'title' => 'Simak Pengertian Perusahaan Asuransi Sebelum Mendaftar',
                'thumbnail' => '/media/image/article/article3.png',
                'content' => '<p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Jika bicara tentang asuransi memang tidak akan pernah
                ada habisnya, mengingat asuransi sendiri sangat dibutuhkan oleh sebagian besar
                masyarakat. Dan untuk sekarang ini memang ada banyak sekali beberapa penjelasan
                mengenai perusahaan asuransi di internet, dan jika Anda lihat sepintas, memang
                sepertinya tidak sama, hal tersebut mungkin saja disebabkan dari pendapat
                masing – masing yang mengartikan perusahaan asuransi itu sendiri, namun untuk
                semua inti maupun maksud penjelasan tersebut semuanya sama. Dan jika memang
                Anda ingin mengetahui mengenai pengertian perusahaan asuransi dan beberapa hal
                yang terkait dengan hal tersebut, maka pada kesempatan kali ini kita akan
                membahasnya.</span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID"><br></span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">Tentang Perusahaan Asuransi</span></strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Lalu apa yang dimaksud dengan perusahaan asuransi?
                Pada dasarnya yang dimaksud dengan hal tersebut merupakan sebuah lembaga yang
                mana menyediakan beberapa pilihan polis asuransi yang bertujuan untuk
                melindungi seseorang, terutama nasabah dari perusahaan asuransi tersebut dari
                berbagai resiko kerugian yang bisa saja terjadi kapan saja dengan cara
                membayarkan premi dengan teratur. perusahaan asuransi sendiri bekerja dengan
                sistem menyatukan resiko dari beberapa pemegang polis asuransi. Contohnya saja
                seperti AIA Financial, Manulife, Sinarmas, Prudential, Allianz dan masih banyak
                lagi yang lainnya.</span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID"><br></span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">Tentang Polis Asuransi</span></strong><span lang="EN-ID" style="font-size:
                12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Kemudian apa yang dimaksud dengan polis asuransi?
                Polis asuransi sendiri merupakan sebuah perjanjian asuransi maupun
                pertanggungan yang mana sifatnya konsensual ataupun sudah terdapat kesepakatan.
                Polis asuransi tersebut juga harus dibuat dengan tertuli di dalam sebuah akta
                di antara beberapa pihak yang mengadakan perjanjian. Dan akta tertulis yang
                sudah disepakati tersebut yang dinamakan polis asuransi. Dimana bisa juga
                diartikan sebagai sebuah tanda bukti secara tertulis tentang kesepakatan di
                antara pihak tertanggung dan juga penanggung.</span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID"><br></span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">
                </p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">Jenis – Jenis Asuransi</span></strong></p><p class="MsoNormal"><span lang="EN-ID" style="font-size:
                12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span><span style="font-family: &quot;Times New Roman&quot;, serif; font-size: 12pt; text-align: justify;">Setelah mengetahui
                pengertian perusahaan asuransi tersebut, Anda juga sangat perlu untuk mengetahui
                apa saja jenis dari asuransi itu sendiri. Dan berikut ini adalah beberapa jenis
                asuransi yang bisa Anda ketahui:</span></p>

                <ol style="margin-top:0cm" start="1" type="1">
                 <li class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Yang pertama adalah asuransi jiwa, dimana bisa
                     memberikan keuntungan finansial untuk orang yang mana sudah ditunjuk atas
                     kematian dari pihak tertanggung. Perusahaan yang mana menyediakan asuransi
                     jiwa tersebut nantinya akan membayar setelah pihak dari tertanggung sudah
                     meninggal dunia, dan sebagian perusahaan asuransi dapat memungkinkan jika
                     pihak tertanggung bisa melakukan klaim dana sebelum meninggal dunia.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                     mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></li>
                 <li class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Kemudian yang kedua adalah asuransi kesehatan,
                     asuransi ini merupakan sebuah produk asuransi yang sengaja ditujukan untuk
                     menangani beberapa masalah kesehatan yang bisa ditimbulkan oleh suatu
                     penyakit, serta menanggung proses dari perawatan pada pihak tertanggung.
                     Asuransi ini sendiri biasanya digunakan untuk melindungi tertanggung pada
                     resiko cidera, kemudian sakit, lalu cacat atau bahkan kematian yang
                     ditimbulkan oleh sebuah kecelakaan. Asuransi ini senidri dapat dibeli
                     untuk diri sendiri ataupun orang lain bahkan untuk anggota keluarga.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                     mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></li>
                 <li class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Lalu yang ketiga adalah asuransi pendidikan,
                     dimana asuransi ini akan memberikan asuransi dar orang tua kepada anak –
                     anaknya, dan untuk biaya yang harus dibayarkan oleh pihak tertanggung juga
                     tergantung dengan jenis pendidikan yang mana ingin didapatkan nantinya.
                     Asuransi ini sendiri merupakan sebuah solusi yang sangat baik untuk masa
                     depan anak.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:
                     &quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
                     mso-fareast-language:EN-ID"><o:p></o:p></span></li>
                 <li class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Selain itu ada juga asuransi kendaraan, yang
                     merupakan sebuah jenis asuransi yang terbilang paling populer sekarang
                     ini. Asuransi ini akan menanggung kerusakan pada kendaraan sendiri maupun
                     orang lain yang ditimbulkan oleh sebuah kecelakaan, baik itu ringan
                     ataupun berat. Bahkan asuransi juga akan memberikan perlindungan bagi
                     nasabah yang kehilangan kendaraannya.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:
                     &quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></li>
                </ol>',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'title' => 'Jenis Asuransi dan Peran Asuransi Bagi Kehidupan Anda',
                'thumbnail' => '/media/image/article/article4.png',
                'content' => '<p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Disadari atau tidak hidup kita tidak akan lepas dari
                yang namanya risiko yang mungkin terjadi di masa depan. Oleh karena itu, kita
                perlu mengantisipasi jika sesuatu hal yang tidak kita inginkan terjadi agar
                kita siap untuk menghadapi. Nah, solusi untuk mengatasi masalah ini adalah
                dengan ikut program asuransi. Peran asuransi sangat besar dalam kehidupan kita
                karena asuransi merupakan salah satu solusi proteksi diri terhadap berbagai macam
                risiko.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Pada saat ini ada banyak tersedia perusahaan asuransi
                di Indonesia yang menawarkan berbagai macam jenis asuransi sebagai produknya.
                Bahkan, saat ini ada juga perusahaan asuransi syariah yang kegiatannya sesuai
                dengan syariat islam.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Nah, untuk lebih jelasnya berikut ini akan kami
                jelaskan beberapa jenis asuransi dan juga peran asuransi untuk kehidupan Anda
                berdasarkan jenisnya.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">





                <strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;mso-fareast-font-family:
                Calibri;mso-fareast-theme-font:minor-latin;mso-ansi-language:EN-ID;mso-fareast-language:
                EN-US;mso-bidi-language:AR-SA">Jenis Jenis Asuransi dan Peran Asuransi</span></strong></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;mso-fareast-font-family:
                Calibri;mso-fareast-theme-font:minor-latin;mso-ansi-language:EN-ID;mso-fareast-language:
                EN-US;mso-bidi-language:AR-SA"><br></span></strong></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Berikut ini jenis jenis asuransi yang secara umum
                disediakan oleh perusahaan asuransi. Anda bisa memilihnya sesuai dengan
                keperluan dan kebutuhan Anda dan keluarga. </span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">1. Asuransi Jiwa</span></strong><span lang="EN-ID" style="font-size:12.0pt;
                line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Jenis asuransi yang satu ini memiliki manfaat
                asuransi yakni dapat memberikan keuntungan finansial pada tertanggung yang
                meninggal dunia. Asuransi jiwa ini dapat Anda beli untuk kepentingan diri
                sendiri atas nama tertanggung saja atau bisa juga dibeli untuk kepentingan
                orang ketiga.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Jadi, misal seorang suami bisa membeli asuransi jiwa
                yang dapat memberikan manfaat asuransi kepadanya setelah kematian sang istri.
                Begitu juga orang tua yang dapat mengasuransikan dirinya terhadap kematian sang
                anak.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">2. Asuransi Kesehatan</span></strong><span lang="EN-ID" style="font-size:
                12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Jenis asuransi ini adalah produk asuransi yang akan
                menangani masalah kesehatan tertanggung karena suatu penyakit serta menanggung
                segala biaya proses perawatan.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">3. Asuransi Kendaraan</span></strong><span lang="EN-ID" style="font-size:
                12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID"><a href="https://www.disitu.com/Asuransi/Asuransi-Mobil"><span style="mso-fareast-font-family:
                &quot;Times New Roman&quot;;mso-fareast-theme-font:major-fareast;color:black;mso-color-alt:
                windowtext">Asuransi kendaraan</span></a>&nbsp;merupakan jenis asuransi yang
                berfokus pada tanggungan cedera orang lain atau terhadap kendaraan orang lain
                yang disebabkan oleh si tertanggung. Tidak hanya itu, asuransi kecelakaan ini
                juga dapat digunakan untuk membayar kerugian kerusakan atau kehilangan pada
                kendaraan bermotor tertanggung. </span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">4. Asuransi Pendidikan</span></strong><span lang="EN-ID" style="font-size:
                12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Sesuai dengan namanya, jenis asuransi ini juga sangat
                populer di Indonesia dan menjadi favorit di kalangan para pemegang polis. Jadi,
                asuransi pendidikan biasanya akan dijadikan alternatif terbaik dan solusi untuk
                menjamin kehidupan yang lebih baik terutama pada aset pendidikan anak.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Perusahaan asuransi akan menerima premi asuransi
                untuk dikelola. Premi asuransi yang harus diberikan oleh peserta asuransi
                berbeda-beda sesuai dengan tingkatan pendidikan yang ingin mereka dapatkan
                nantinya.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">5. Asuransi Bisnis</span></strong><span lang="EN-ID" style="font-size:12.0pt;
                line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Asuransi bisnis merupakan salah satu asuransi untuk
                memberikan penggantian atau proteksi terhadap kerusakan atau kehilangan
                keuntungan dalam jumlah besar yang mungkin terjadi pada bisnis seseorang.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Jadi pemilik bisnis harus membayarkan premi secara
                rutin sebagai tertanggung. Perusahaan asuransi bertanggung jawab dalam
                mengelola premi asuransi untuk memberikan klaim pada saat tertanggung mengalami
                kerusakan, kehilangan atau kerugian bisnis. Jadi, perusahaan asuransi
                bertanggung jawab terhadap kerugian bisnis tertanggung dengan menerima premi.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">6. Asuransi Kepemilikan Rumah atau Properti</span></strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Terkadang seseorang juga akan memberikan perlindungan
                pada aset yang dinilai cukup berharga seperti rumah atau properti yang
                dimilikinya. Jadi, adanya asuransi ini akan memberikan perlindungan dan
                keringanan jika rumah atau properti tertanggung mengalami musibah misalnya
                seperti kebakaran. </span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Masih banyak jenis jenis asuransi dengan perannya
                masing-masing yang dapat Anda ikuti seperti asuransi kredit,asuransi umum,
                asuransi kelautan dan asuransi perjalanan. Sekarang Anda sudah tahu kan apa
                saja peran asuransi dan jenis asuransi. Jadi, jenis asuransi mana yang akan
                Anda pilih?</span></p><ol style="margin-top:0cm" start="1" type="1">
                </ol>',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'title' => 'Kenali Fungsi Asuransi dan Tujuan Asuransi',
                'thumbnail' => '/media/image/article/article5.png',
                'content' => '<p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Tidak sedikit masyarakat yang masih kurang mengerti
                pentingnya mengikuti asuransi. Bahkan asuransi terkadang juga masih dipandang
                sebelah mata. Padahal asuransi sendiri memiliki banyak manfaat untuk kehidupan
                di masa depan. Bahkan tidak hanya bagi masyarakat yang belum memiliki asuransi,
                masyarakat yang sudah memiliki asuransi pun terkadang masih awam dengan fungsi
                asuransi dan tujuan asuransi yang sebenarnya.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Nah, oleh karena itu untuk lebih jelasnya berikut ini
                akan kami jelaskan mengenai fungsi asuransi dan juga tujuan asuransi yang wajib
                Anda ketahui.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">Fungsi Asuransi yang Utama</span></strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Sebelum membahas mengenai fungsi asuransi, terlebih
                dahulu Anda harus mengetahui apa itu asuransi. Jadi, asuransi merupakan suatu
                perjanjian yang dilakukan oleh nasabah sebagai pihak tertanggung dan perusahaan
                asuransi sebagai pihak penanggung. Adapun perjanjian yang dilakukan berisi
                pernyataan bahwa perusahaan asuransi bersedia membayar kerugian dengan jumlah
                uang tertentu jika terjadi suatu hal di masa depan setelah nasabah menyetujui
                pembayaran atas sejumlah uang atau yang disebut dengan premi asuransi.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Pada umumnya ada banyak produk asuransi yang
                disediakan oleh perusahaan asuransi seperti asuransi pendidikan, asuransi
                kesehatan,&nbsp;<a href="https://www.disitu.com/Asuransi/Asuransi-Mobil"><span style="color:black;mso-color-alt:windowtext">asuransi kendaraan</span></a>,
                asuransi jiwa, asuransi kecelakaan, asuransi kebakaran dan jenis asuransi yang
                lainya. &nbsp;Berikut ini beberapa fungsi asuransi untuk lebih jelasnya.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">1. Sebagai Pengalihan Risiko</span></strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Asuransi merupakan salah satu cara efektif untuk
                menghindari risiko. Hal ini sesuai dengan fungsi asuransi sebagai pengalihan
                risiko yang berarti bahwa segala bentuk risiko dalam hal kerugian akan
                dialihkan kepada perusahaan asuransi selaku pihak penanggung.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Sehingga perusahaan asuransi bertanggung jawab
                mengeluarkan biaya ganti rugi sesuai jumlah ketidakpastian kerugian yang
                diderita oleh nasabah akibat kejadian yang tidak terduga atau biasa disebut
                dengan santunan klaim karena nasabah sudah membayarkan premi secara rutin.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">2. Sebagai Penghimpunan Dana</span></strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Tugas dari perusahaan asuransi sendiri yakni berperan
                untuk mengatur semua dana yang dihimpun dari nasabah dengan cara mengelolanya
                sedemikian rupa agar dapat berkembang. Sehingga nantinya jika terjadi kerugian
                &nbsp;tidak terduga yang diderita oleh nasabah, pihak asuransi akan bertanggung
                jawab untuk membayar ganti rugi sebesar kerugian yang diderita oleh nasabahnya.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">3. Sebagai Penyeimbangan Premi</span></strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Seperti yang sudah dijelaskan sebelumnya, sebagai
                pihak penanggung dan pihak penghimpun dana, perusahaan asuransi memiliki tugas
                untuk mengatur agar premi yang dibayarkan oleh nasabah seimbang dengan risiko
                yang nantinya akan ditanggung olehnya. Dengan begitu, pada akhirnya tidak ada
                salah satu pihak yang merasa dirugikan dengan adanya perjanjian tersebut.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">Tujuan Asuransi</span></strong><span lang="EN-ID" style="font-size:12.0pt;
                line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Sekarang Anda sudah tahu kan apa saja fungsi
                asuransi, selanjutnya kami akan menjelaskan mengenai tujuan asuransi. Adapun
                tujuan utama dari asuransi adalah sebagai berikut.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt 36pt; text-align: justify; text-indent: -18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><!--[if !supportLists]--><span lang="EN-ID" style="font-size:10.0pt;
                mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
                mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span></span><!--[endif]--><span lang="EN-ID">Untuk memberikan jaminan kepada nasabah agar terlindung dari
                risiko-risiko yang mungkin akan di derita di masa depan secara tidak terduga.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt 36pt; text-align: justify; text-indent: -18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><!--[if !supportLists]--><span lang="EN-ID" style="font-size:10.0pt;
                mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
                mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span></span><!--[endif]--><span lang="EN-ID">Pemerataan biaya atas kerugian yang diderita nasabah karena nasabah
                hanya perlu mengeluarkan biaya tertentu dalam bentuk premi asuransi yang
                dibayarkan rutin dan tidak perlu membayar kerugian yang diderita.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt 36pt; text-align: justify; text-indent: -18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><!--[if !supportLists]--><span lang="EN-ID" style="font-size:10.0pt;
                mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
                mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span></span><!--[endif]--><span lang="EN-ID">Meningkatkan efisiensi terhadap suatu hal karena dengan adanya asuransi
                nasabah tidak perlu melakukan upaya pengamanan dan pengawasan.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt 36pt; text-align: justify; text-indent: -18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><!--[if !supportLists]--><span lang="EN-ID" style="font-size:10.0pt;
                mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
                mso-bidi-font-family:Symbol">·<span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span></span><!--[endif]--><span lang="EN-ID">Jenis asuransi jiwa juga memiliki tujuan asuransi yang sangat mulia
                yakni sebagai tabungan nasabah atau investasi nasabah karena jumlah uang yang
                akan diterima oleh nasabah sudah pasti akan jauh lebih besar dibandingkan
                dengan jumlah premi asuransi yang dibayarkan. Namun hal ini hanya berlaku untuk
                produk asuransi jiwa.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">



































                </p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Itu
                dia fungsi dan tujuan asuransi yang perlu Anda ketahui. Tertarik untuk
                mengikuti</span><span lang="EN-ID"><a href="https://www.disitu.com/Asuransi"><span style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; color: black; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">asuransi&nbsp;</span></a></span><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">mulai hari ini? Semoga informasi ini
                bermanfaat buat Anda.</span><span lang="EN-ID" style="font-size:12.0pt;
                font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
                mso-fareast-language:EN-ID"><o:p></o:p></span></p><ol style="margin-top:0cm" start="1" type="1">
                </ol>',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'title' => 'Istilah yang Harus Ketahui Sebelum Memilih Asuransi',
                'thumbnail' => '/media/image/article/article6.jpg',
                'content' => '<p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Kebutuhan asuransi kesehatan ataupun jenis asuransi
                lainnya kini semakin meningkat karena memang masyarakat sudah sadar dengan
                pentingnya fungsi asuransi bagi kesehatan tubuh ataupun dalam keperluan
                investasi. Meskipun banyak kalangan mencari tahu seputar informasi asuransi
                akan tetapi belum banyak yang mengetahui berbagai istilah di dalamnya.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Banyak pakar menilai bahwa
                proses pemilihan asuransi membutuhkan banyak pertimbangan dari sisi
                kredibilitas pelayanan hingga menentukan fasilitas secara lengkap dari sebuah
                lembaga asuransi. Maka dari itu sebelum Anda menentukan jenis asuransi beserta
                melihat fasilitasnya cermati dahulu seperti apa daftar istilah yang wajib
                diketahui oleh calon nasabah asuransi agar ke depannya tidak mengalami masalah.
                Berikut ini ada daftar istilah yang ada di dalam asuransi wajib Anda ketahui.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Premi</span></b><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Istilah pertama adalah premi
                yang mana banyak ditemukan di setiap perusahaan asuransi. Meksipun masih
                familiar akan tetapi masih banyak yang belum mengerti seperti apa arti dan
                fungsinya. Premi adalah sejumlah uang atau dana yang harus dibayarkan pihak
                bertanggung dalam artian nasabah kepada pihak penanggung jawab yakni pihak
                perusahaan asuransi dengan mempertimbangkan segala kondisi terutama kesehatan
                pihak tertanggung.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:
                &quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:
                EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Underwriting</span></b><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Berikutnya ada
                Underwriting yang mana menjadi salah satu penjaminan dengan fungsi evaluasi
                hingga menyeleksi bahkan bertanggung jawab terhadap proses menilai hingga
                menafsirkan resiko asuransi.</span><span lang="EN-ID" style="font-size:12.0pt;
                font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
                mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Polis Asuransi</span></b><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Masih ada istilah lain
                yakni polis asuransi yang mana sering disebut dengan perjanjian asuransi
                ataupun kontrak asuransi. Dari sinilah pihak tertanggung dan penanggung hingga
                pengajuan dokumen lain selalu memperhatikan satu kesatuan hingga disebut
                sebagai perjanjian asuransi.</span><span lang="EN-ID" style="font-size:12.0pt;
                font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
                mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Lapse</span></b><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Dalam bidang asuransi juga
                terdapat istilah penting yakni lapse yang mana menjadi salah satu kondisi
                dimana status dari polis asuransi dalam keadaan tidak aktif ataupun pada masa
                tenggang karena terlambat hingga tidak membayar premi. Rata-rata masa tenggang
                dari lapse ini mencapai 45 hari hingga akhirnya status nasabah kembali aktif
                jika sudah membayar.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:
                &quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:
                EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Rider</span></b><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Setelah lapse ada istilah
                lain yakni rider asuransi ataupun sering disebut dengan asuransi tambahan. Dari
                rider satu ini akan terjadi bila nasabah membeli asuransi dasarnya, sedangkan
                tujuan dari rider asuransi ini adalah memberikan proteksi keluarga pada saat kondisi
                situasional.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Anuitas</span></b><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Mungkin Anda sudah pernah
                mendengar tentang istilah anuitas yang mana menjadi salah satu rangkaian
                penerimaan hingga pembayaran tetap dan masih dilakukan secara berkala dengan
                jangka waktu yang sudah ditentukan. Rata-rata anuitas dilakukan oleh perusahaan
                terhadap nasabah dalam bentuk pembayaran sesuai premi hingga masa kontrak
                status nasabah. Jadi anuitas bisa dikatakan sebagai salah satu imbalan premi
                yang sudah dibayarkan pihak perusahaan asuransi untuk memberikan pelayanan
                terbaik kepada nasabah.</span><span lang="EN-ID" style="font-size:12.0pt;
                font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
                mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Aktuaris</span></b><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Berikutnya ada aktuaris
                yang mana menjadi salah satu ahli dalam bidang statistika hingga matematika
                yang mana tugasnya menentukan biaya premi hingga seperti apa resiko didalamnya.
                Jadi tugas dari seorang aktuaris sendiri adalah memberikan gambaran mengenai
                estimasi mengenai resiko yang akan dijamin ataupun dari sisi metode
                perhitungannya kepada calon nasabah asuransi.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:
                &quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><b><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Policy Loan</span></b><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Terakhir ada Policy Loan
                ataupun disebut dengan pinjaman polis yang sampai sekarang bisa diakses oleh
                setiap nasabah yang sudah memiliki nilai tunai dalam perhitungan preminya.
                Rata-rata gadai polis ini diakses oleh nasabah yang sudah lama menyandang
                status sebagai nasabah hingga akhirnya bisa memperoleh pinjaman polis.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">























































                </p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Jadi itulah berbagai macam
                daftar istilah penting dalam asuransi yang sampai detik ini benar-benar
                memberikan gambaran seperti apa fungsi hingga hal-hal penting di dalam proses
                klaim ataupun pendaftaran sebagai nasabah perusahaan asuransi.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><ol style="margin-top:0cm" start="1" type="1">
                </ol>',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'title' => 'Info Asuransi Dasar yang Harus Anda Pahami',
                'thumbnail' => '/media/image/article/article7.png',
                'content' => '<p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Sekarang ini banyak orang yang menawarkan asuransi.
                Berbagai jenis ditawarkan agar membuat orang-orang tertarik untuk mengambilnya.
                Namun sebagian lagi masih kebingungan dengan keuntungan yang bisa mereka
                dapatkan nantinya. Itu berarti masih kurangnya pemahaman orang-orang tentang
                asuransi. Padahal jika sudah mengetahui alur kerjanya akan banyak keuntungan
                yang bisa didapatkan. Untuk itu lebih baik ketahui info asuransi dasar yang
                bisa membuat Anda yakin untuk mengambilnya.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Setiap perusahaan penyedia jasa asuransi biasanya
                akan menawarkan banyak pilihan. Juga menjelaskan setiap prosesnya dengan
                istilah yang masih awam. Tak hanya itu, kegunaan yang tidak spesifik pun
                menjadikan banyak orang kebingungan. Untuk itu perhatikan info asuransi berikut
                agar Anda paham maksudnya :</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">- Memahami istilah dalam asuransi</span></strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Biasanya yang akan membuat bingung yaitu
                istilah-istilah yang asing digunakan. Untuk mengambil asuransi, calon nasabah
                harus banyak mencari info mengenai pemakaian istilah ini. Dengan begitu akan
                mudah paham maksud dan tujuan dari asuransi yang di tawarkan. Istilah Premi
                akan selalu Anda dengar dalam asuransi karena ini merupakan pembayaran iuran
                yang harus dibayarkan setiap bulannya.</span><span lang="EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Kemudian Anda akan diberikan polis yaitu sebuah
                perjanjian tertulis untuk melakukan kerjasama ini. Nantinya akan diberikan di
                awal sebagai kontrak asuransi secara resmi. Info asuransi berikutnya yang harus
                diketahui yaitu memahami Klaim asuransi sebagai bentuk permintaan resmi untuk
                membayar kerugian atas kecelakaan yang terjadi sesuai ketentuan pada polis yang
                telah disepakati.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">- Membaca perjanjian dengan detail</span></strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID">Untuk memahami asuransi penting sekali membaca polis
                dengan benar. Karena nantinya semua ketentuan mengenai apa yang akan di dapat
                dan berapa biaya yang harus Anda keluarkan tiap bulan sudah tercantum. Dalam
                perjanjian akan terlihat asuransi apa yang akan di ambil. Jangan sampai ketika
                mengajukan klaim ada berberapa ketentuan yang dirasa tidak sesuai kebutuhan.
                Padahal sudah dijelaskan sejak awal dalam lembaran polis yang ditandatangani.</span><span lang="EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">- Terdapat berbagai jenis asuransi</span></strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">info asuransi berikutnya
                yang wajib diketahui adalah mengenai jenis asuransi. Dari mulai asuransi jiwa,
                kesehatan, pendidika, kecelakaan dan lain-lain. Pilih jenis asuransi yang
                dirasa memang memiliki manfaat besar untuk kedepannya. Yaitu dapat meringankan
                keselematan pribadi dan keluarga. Juga sebagai bentuk tabungan masa depan yang
                bisa di ambil ketika benar-benar dibutuhkan sesuai dengan perjanjian awal.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal"><strong><span lang="EN-ID" style="font-size:12.0pt;line-height:
                107%">- Asuransi tidak termasuk investasi</span></strong><span lang="EN-ID" style="font-size:12.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Dan terakhir jangan
                samakan asuransi dengan investasi. Karena ini adalah bentuk dua hal yang berbeda
                fungsinya. Investasi merupakan aset atau tabungan untuk masa depan. Tetapi bisa
                digunakan sewaktu-waktu sesuai dengan keinginan sendiri. Untuk keuntungan
                investasi bisa di prediksi dengan perhitungan yang di buat sendiri</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Tetapi investasi merupakan
                aset untuk kebutuhan finansial yang bisa digunakan pada waktu yang ditetapkan.
                Yaitu ketika mengalami kondisi darurat sesuai jenis asuransi yang di pilih.
                Misalkan ketika terjadi kecelakaan, sakit atau untuk biaya sekolah maka
                asuransi dapat berfungsi. Ini merupakan bentuk perlindungan untuk diri sendiri
                dan keluarga yang menjadi tanggungan dalam asuransi yang di buat.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><p style="margin: 0cm 0cm 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">

























                </p><p class="MsoNormal" style="margin-bottom: 7.5pt; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">Ada banyak berbagai
                info&nbsp;</span><span lang="EN-ID"><a href="https://www.disitu.com/Asuransi/Asuransi-Mobil"><span style="font-size:
                12.0pt;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
                color:black;mso-color-alt:windowtext;mso-fareast-language:EN-ID">asuransi</span></a></span><span lang="EN-ID" style="font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;">&nbsp;yang harus Anda pelajari sebelum mendaftarkan
                diri. Ini sebagai bentuk penjelasan yang terarah agar Anda tahu apa yang harus
                di pilih sesuai kebutuhan. Perhatikan jenis asuransi mana yang termasuk
                mendesak agar manfaatnya lebih terasa. Anda bisa memahaminya dengan membaca
                melalui perusahaan asuransi online yang lebih mudah di akses. Atau bisa juga
                langsung mendatangi ke tempat asuransi untuk mendengar berbagai penjelasan
                terkait.</span><span lang="EN-ID" style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,serif;
                mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-language:EN-ID"><o:p></o:p></span></p><ol style="margin-top:0cm" start="1" type="1">
                </ol>',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);

        DB::table('faqs')->insert([
            [
                'question' => 'Apa itu Easurance?',
                'answer' => 'Easurance merupakan portal yang menyadiakan imformasi dari berbagai  macam asuransi. Anda diberikan kemudahan melihat premi dan manfaat asuransi yang Anda butuhkan.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Pilihan apa saja yang ada di Easurance?',
                'answer' => 'Easurance menyediakan pilihan asuransi mobil, asuransi properti dan asuransi perjalanan.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Penyedia asuransi apa saja yang ada di Easurance?',
                'answer' => 'Terdapat beberapa penyedia asuransi yang saat ini ada di Easurance yaitu AXA, ACA, MPM, dan Reliance',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apakah dapat melakukan pembelian polis melalui Easurance?',
                'answer' => 'Pada Easurance belum dapat melakukan pembelian polis asuransi',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apa itu smart search?',
                'answer' => 'Merupakan fitur yang membantu anda dalam menemukan asuransi yang sesuai dengan kebutuhan anda',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apa itu wishlist?',
                'answer' => 'Merupakan fitur yang membantu anda dalam menyimpan hasil pencarian asuransi',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apa yang dimaksud dengan TLO?',
                'answer' => 'TLO, pertanggungan hanya terbatas pada kehilangan dan juga penggantian biaya perbaikan kalau jumlahnya mecapai lebih dari 75 persen harga pertanggungan (kendaraan yang diasuransikan)',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apa yang dimaksud dengan Comprehensive?',
                'answer' => 'Comprehensive menanggung kerugian akibat kerusakan sebagian (partial loss) atau kerusakan dan kehilangan total (total loss).',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apa yang dimaksut dengan Additional Coverage pada Car Insurance?',
                'answer' => 'Hal-hal tambahan apa saja yang diinginkan user ter-cover asuransi seperti tambahan asuransi untuk supir, penumpang, risiko  banjir atau badai, dll.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apakah yang dimaksud dengan ATPM?',
                'answer' => 'ATPM (Agen Tunggal Pemegang Merk) adalah bengkel khusus yang menangani kendaraan dengan kebutuhan khusus atau sparepart khusus dengan merk tertentu',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apakah yang dimaksud dengan TPL?',
                'answer' => 'TPL (Third Party Liability) merupakan perlindungan terhadap pihak ketiga yang terlibat dalam kecelakaan.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apakah yang dimaksud dengan PA Driver?',
                'answer' => 'PA Driver merupakan perlindungan tambahan untuk supir pengendara mobil tersebut',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apakah yang dimaksud dengan PA Passenger?',
                'answer' => 'PA Passenger merupakan perlindungan tambahan untuk penumpang dalam mobil tersebut',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apakah yang dimaksud dengan SRCC, TSFWD, EQVET, dan TS?',
                'answer' => 'SRCC, TSFWD, EQVET, dan TS merupakan pilihan tambahan dalam perlindungan asuransi',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apa itu SRCC?',
                'answer' => 'Merupakan pilihan tambahan perlindungan asuransi dari risiko terjadinya huru-Hara',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apa itu TSFWD?',
                'answer' => 'Merupakan pilihan tambahan perlindungan asuransi dari risiko terjadinya banjir atau badai',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apa itu EQVET?',
                'answer' => 'Merupakan pilihan tambahan perlindungan asuransi dari risiko terjadinya gempa bumi, letusan gunung berapi, dan tsunami. ',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'question' => 'Apa itu TS?',
                'answer' => 'Merupakan pilihan tambahan perlindungan asuransi dari risiko terjadinya terorisme dan Sabotase',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);

        DB::table('admins')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@domain.com',
                'password' => Hash::make('qweqwe'),
            ]
        ]);
    }
}
