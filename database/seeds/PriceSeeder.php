<?php

use Illuminate\Database\Seeder;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_prices')->insert([
            [
                "year" => "2019",
                "price" => "157500000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "141750000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "127575000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "140000000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "130000000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "120000000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "110000000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "100000000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "86000000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "80000000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "75000000",
                "car_series_id" => "1", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "173500000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "156150000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "140535000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "145000000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "130000000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "120000000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "110000000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "100000000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "82000000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "76000000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "72000000",
                "car_series_id" => "2", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "252900000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "229900000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "209000000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "190000000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "183000000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "166000000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "159000000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "150000000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "147000000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "135000000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "124000000",
                "car_series_id" => "3", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "380900000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "354500000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "325000000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "280000000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "199000000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "190000000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "183000000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "161000000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "136000000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "125000000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "115000000",
                "car_series_id" => "4", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "398000000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "371700000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "325000000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "250000000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "230000000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "215000000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "202000000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "181000000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "143000000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "135000000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "133000000",
                "car_series_id" => "5", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "445500000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "439000000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "386000000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "342000000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "320000000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "235000000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "215000000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "162000000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "147000000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "145000000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "130000000",
                "car_series_id" => "6", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "172800000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "165000000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "145000000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "139500000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "130000000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "120000000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "107000000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "95000000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "90000000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "81000000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "65000000",
                "car_series_id" => "7", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "193000000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "172000000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "151000000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "134000000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "130000000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "118000000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "112000000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "105000000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "98500000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "88650000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "79785000",
                "car_series_id" => "8", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "198000000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "193000000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "170000000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "150000000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "134000000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "104000000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "101000000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "98000000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "93000000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "87500000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "78750000",
                "car_series_id" => "9", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "204100000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "204000000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "180000000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "159000000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "141000000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "106000000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "103000000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "99000000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "93500000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "92000000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "89000000",
                "car_series_id" => "10", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "314000000",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "290000000",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "273000000",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "259000000",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "245000000",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "220000000",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "205000000",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "184500000",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "166050000",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "149445000",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "134500500",
                "car_series_id" => "11", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "102950000",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "99900000",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "97000000",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "87000000",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "84000000",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "77000000",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "75000000",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "71500000",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "64350000",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "57915000",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "52123500",
                "car_series_id" => "12", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "103000000",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "97000000",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "95000000",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "85000000",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "81000000",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "71000000",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "69000000",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "62100000",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "55890000",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "50301000",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "45270900",
                "car_series_id" => "13", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "112000000",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "102900000",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "95000000",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "88000000",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "83000000",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "73000000",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "68000000",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "61200000",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "55080000",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "49572000",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "44614800",
                "car_series_id" => "14", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "331400000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "326500000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "293850000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "232500000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "199000000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "179000000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "155000000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "148000000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "139000000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "131000000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "123000000",
                "car_series_id" => "15", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "259500000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "235950000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "214500000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "195000000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "175000000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "162500000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "140000000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "135000000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "125000000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "120000000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "107000000",
                "car_series_id" => "16", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "560000000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "410000000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "369000000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "332100000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "235000000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "210000000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "170000000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "162500000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "155000000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "149000000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "129500000",
                "car_series_id" => "17", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "366400000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "366400000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "334500000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "250000000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "232000000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "223000000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "220000000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "196000000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "174000000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "155000000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "138000000",
                "car_series_id" => "18", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "381800000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "349500000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "270000000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "245000000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "231000000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "220000000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "216000000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "192000000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "171000000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "152000000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "135000000",
                "car_series_id" => "19", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "209330000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "190300000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "173000000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "135000000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "110000000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "104000000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "102000000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "97000000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "92000000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "88000000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "75000000",
                "car_series_id" => "20", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "261360000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "237600000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "216000000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "135000000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "130000000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "128000000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "110000000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "100000000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "92000000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "88000000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "86000000",
                "car_series_id" => "21", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "187000000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "170000000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "155000000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "140000000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "134000000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "126000000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "115000000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "103000000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "97000000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "91000000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "89500000",
                "car_series_id" => "22", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "147500000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "125000000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "115000000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "109000000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "99000000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "98000000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "95000000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "90000000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "81000000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "72900000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "65610000",
                "car_series_id" => "23", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "162500000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "159000000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "143000000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "124000000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "119000000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "107000000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "105000000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "101000000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "90900000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "81810000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "73629000",
                "car_series_id" => "24", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "165000000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "163000000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "147000000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "127000000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "120000000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "115000000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "110000000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "103000000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "92700000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "83430000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "75087000",
                "car_series_id" => "25", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "191000000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "178000000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "160000000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "139000000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "136000000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "129500000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "120000000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "117000000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "112500000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "108000000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "97200000",
                "car_series_id" => "26", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "260000000",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "249000000",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "212000000",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "194000000",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "184000000",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "165600000",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "149040000",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "134136000",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "120722400",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "108650160",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "97785144",
                "car_series_id" => "27", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "314000000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "306000000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "233000000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "233000000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "226000000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "220000000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "210000000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "195000000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "175500000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "157950000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "142155000",
                "car_series_id" => "28", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "401500000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "379000000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "315000000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "295000000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "270000000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "260000000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "255000000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "239000000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "230000000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "226000000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "199000000",
                "car_series_id" => "29", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "222800000",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "214000000",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "190000000",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "165000000",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "151000000",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "131000000",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "129500000",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "116550000",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "104895000",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "94405500",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "84964950",
                "car_series_id" => "30", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "233800000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "225000000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "200000000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "173000000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "155000000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "122000000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "120000000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "108000000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "97200000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "87480000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "78732000",
                "car_series_id" => "31", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "246000000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "236000000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "211000000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "183000000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "165000000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "133000000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "128000000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "125000000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "112500000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "85000000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "76500000",
                "car_series_id" => "32", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "256750000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "247000000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "221000000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "191000000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "172000000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "170400000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "168000000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "150000000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "145000000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "136000000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "130000000",
                "car_series_id" => "33", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "120000000",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "116000000",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "104000000",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "91000000",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "81000000",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "74000000",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "71000000",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "63900000",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "57510000",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "51759000",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "46583100",
                "car_series_id" => "34", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "135800000",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "128000000",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "114000000",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "100000000",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "77000000",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "70000000",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "67000000",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "60300000",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "54270000",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "48843000",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "43958700",
                "car_series_id" => "35", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "174550000",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "132000000",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "118000000",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "103000000",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "88000000",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "81000000",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "76000000",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "68400000",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "61560000",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "55404000",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "49863600",
                "car_series_id" => "36", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "187650000",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "148000000",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "132000000",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "115000000",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "85000000",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "80000000",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "76000000",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "68400000",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "61560000",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "55404000",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "49863600",
                "car_series_id" => "37", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "216500000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "180000000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "130000000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "127000000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "116000000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "115000000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "112000000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "102000000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "98000000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "88200000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "79380000",
                "car_series_id" => "38", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "221700000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "196950000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "140000000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "138500000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "133000000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "129000000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "122000000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "115000000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "109000000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "87000000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "83000000",
                "car_series_id" => "39", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "233300000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "160200000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "155000000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "148000000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "138000000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "136000000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "131000000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "115000000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "106000000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "96500000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "89000000",
                "car_series_id" => "40", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "234650000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "208950000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "188000000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "157000000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "155000000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "142000000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "133000000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "119000000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "107000000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "94000000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "80000000",
                "car_series_id" => "41", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "629950000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "566955000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "509000000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "329000000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "295000000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "245000000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "239000000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "201000000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "163000000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "147000000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "132000000",
                "car_series_id" => "42", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "646650000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "622000000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "554000000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "453000000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "399000000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "312000000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "296000000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "256000000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "235000000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "185000000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "168000000",
                "car_series_id" => "43", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "800000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "790000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "703000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "575000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "506000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "363000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "305000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "275000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "270000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "225000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "175000000",
                "car_series_id" => "44", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "235400000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "199250000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "185000000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "165000000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "145000000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "133000000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "130000000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "118000000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "108000000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "103000000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "98000000",
                "car_series_id" => "45", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "247700000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "209450000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "175000000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "153000000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "142000000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "135000000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "128000000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "116000000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "107000000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "102000000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "97000000",
                "car_series_id" => "46", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "264100000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "250000000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "211650000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "202000000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "177500000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "154000000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "136000000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "136000000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "124000000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "128000000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "106000000",
                "car_series_id" => "47", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "275900000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "250000000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "215000000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "185000000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "183000000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "165000000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "139000000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "135000000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "127000000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "129000000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "116000000",
                "car_series_id" => "48", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "204100000",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "201000000",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "177000000",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "159300000",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "143370000",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "129033000",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "116129700",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "104516730",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "94065057",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "84658551.299999997",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "76192696.170000002",
                "car_series_id" => "49", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "227600000",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "225000000",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "192000000",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "189000000",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "180000000",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "162000000",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "145800000",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "131220000",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "118098000",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "106288200",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "95659380",
                "car_series_id" => "50", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "238000000",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "235000000",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "201000000",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "199500000",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "195000000",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "175500000",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "157950000",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "142155000",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "127939500",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "115145550",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "103630995",
                "car_series_id" => "51", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "240200000",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "237000000",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "211000000",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "205000000",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "190000000",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "169900000",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "152910000",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "137619000",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "123857100",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "111471390",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "100324251",
                "car_series_id" => "52", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "252200000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "247000000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "220000000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "219000000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "215000000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "210000000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "200000000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "175000000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "157500000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "141750000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "127575000",
                "car_series_id" => "53", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "261900000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "256000000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "227000000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "225000000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "220000000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "217000000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "213000000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "206100000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "202550000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "146000000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "131400000",
                "car_series_id" => "54", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "175000000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "173000000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "149000000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "130000000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "120000000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "112000000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "98000000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "81000000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "79500000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "75000000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "69900000",
                "car_series_id" => "55", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "203500000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "185000000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "158000000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "139000000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "129000000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "112000000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "107000000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "85000000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "83000000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "70000000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "63000000",
                "car_series_id" => "56", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "217800000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "198000000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "168000000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "147000000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "140000000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "134000000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "110000000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "102000000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "98000000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "90000000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "81000000",
                "car_series_id" => "57", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "510000000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "498950000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "450000000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "370000000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "320000000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "268000000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "253000000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "246000000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "237000000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "213000000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "201000000",
                "car_series_id" => "58", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "375000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "350000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "320000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "290000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "288000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "279000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "255000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "238000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "218000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "215000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "198000000",
                "car_series_id" => "59", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "480000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "465000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "428000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "358000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "346000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "381000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "257000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "248000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "243000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "232000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "211000000",
                "car_series_id" => "60", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2019",
                "price" => "574500000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2018",
                "price" => "470000000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2017",
                "price" => "430000000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2016",
                "price" => "399000000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2015",
                "price" => "355000000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2014",
                "price" => "324000000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2013",
                "price" => "280000000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2012",
                "price" => "276000000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2011",
                "price" => "272000000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2010",
                "price" => "265000000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                "year" => "2009",
                "price" => "260000000",
                "car_series_id" => "61", 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);
    }
}
