<?php

use Illuminate\Database\Seeder;

class DestinationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Destinasi Perjalanan
        DB::table('travel_destinations')->insert([
            [
                'name' => 'DKI Jakarta',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Bali',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Banten',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Bengkulu',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'DIY Yogyakarta',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Gorontalo',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Jambi',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Jawa Barat',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Jawa Tengah',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Jawa Timur',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kalimantan Barat',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kalimantan Selatan',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kalimantan Tengah',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kalimantan Timur',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kep. Babel',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kep. Riau',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Lampung',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Maluku',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Maluku Utara',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Nangroe Aceh Darussalam',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Nusa Tenggara Barat',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Nusa Tenggara Timur',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Nusa Tenggara Tengah',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Papua',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Papua Barat',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Riau',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sulawesi Barat',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sulawesi Selatan',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sulawesi Tengah',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sulawesi Tenggara',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sulawesi Utara',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sumatera Barat',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sumatera Selatan',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sumatera Utara',
                'type' => 'domestic',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Algeria',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Angola',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Benin',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Botswana',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Burkina Faso',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Burundi',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Cameroon',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Cape Verde',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Centra African Republic',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Chad',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Comoros',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Democratic Republic of the Congo',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Djibouti',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Egypt',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Equatorial Guinea',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Eritrea',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Ethiopia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Gabon',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Ghana',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Guinea',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Guinea-Bissau',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Ivory Coast',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kenya',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Lesotho',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Liberia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Libya',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Madagascar',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Malawi',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Mali',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Mauritania',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Mauritius',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Morocco',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Mozambique',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Namibia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Niger',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Nigeria',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Republic of the Congo',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Rwanda',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'São Tomé and Príncipe',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Senegal',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Seychelles',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sierra Leone',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Somalia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'South Africa',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'South Sudan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sudan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Swaziland',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Tanzania',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'The Gambia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Togo',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Tunisia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Uganda',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Zambia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Zimbabwe',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Afghanistan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Armenia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Azerbaijan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Bahrain',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Bangladesh',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Bhutan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Brunei',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Cambodia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'China',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'East Timor',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Georgia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'India',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Iran',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Irag',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Israel',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Japan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Jordan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kazakhstan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kuwait',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kyrgyzstan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Laso',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Lebanon',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Malaysia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Maldives',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Mongolia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Myanmar',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Nepal',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'North Korea',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Oman',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Pakistan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Palestinian Territories',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Philipines',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Qatar',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Saudi Arabia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Singapore',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'South Korea',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sri Lanka',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Syria',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Taiwan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Tajikistan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Thailand',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Turkey',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Turkmenistan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'United Arab Emirates',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Uzbekistan',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Vietnam',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Yemen',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Australia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Federated States of Micronesia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Fiji',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Kiribati',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Marshall Islands',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Nauru',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'New Zealand',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Palau',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Papua New Guinea',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Samoa',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Solomon Islands',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Tonga',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Tuvalu',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Albania',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Andorra',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Austria',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Belarus',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Belgium',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Bosnia and Herzegovina',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Bulgaria',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Croatia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Cyprus',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Czech Republic',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Denmark',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Estonia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Finland',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'France',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Germany',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Greece',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Hungary',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Iceland',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Italy',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Latvia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Liechtenstein',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Lithuania',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Luxembourg',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Malta',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Moldova',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Monaco',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Netherlands',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Norway',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Poland',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Portugal',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Republic of Ireland',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Republic of Macedonia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Romania',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Russia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'San Marino',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Serbia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Slovakia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Slovenia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Spain',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sweden',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Switzerland',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Ukraine',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'United Kingdom',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Vatican City',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Antigua and Barbuda',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Barbados',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Belize',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Canada',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Costa Rica',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Cuba',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Dominica',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Dominican Republic',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'El Salvador',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Grenada',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Guatemala',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Haiti',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Honduras',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Jamaica',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Mexico',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Nicaragua',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Panama',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Saint Lucia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'The Bahamas',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'United States of America',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Saint Kitts and Nevis',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Saint Vincent and Grenadines',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Trinidad and Tobago',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Argentina',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Bolivia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Brazil',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Chile',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Colombia',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Ecuador',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Guyana',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Paraguay',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Peru',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Suriname',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Uruguay',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Venezuela',
                'type' => 'international',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ]
        ]);
    }
}
