<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_types')->insert([
            [
                'name' => 'AVEO ',
                'car_brand_id' => '1', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'CAPTIVA ',
                'car_brand_id' => '1', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'SPIN ',
                'car_brand_id' => '1', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'TRAX',
                'car_brand_id' => '1', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'DATSUN GO ',
                'car_brand_id' => '2', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'DATSUN GO+ ',
                'car_brand_id' => '2', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'ESCAPE',
                'car_brand_id' => '3', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'EVEREST',
                'car_brand_id' => '3', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'FIESTA ',
                'car_brand_id' => '3', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'BRIO ',
                'car_brand_id' => '4', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'HRV ',
                'car_brand_id' => '4', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'MOBILIO ',
                'car_brand_id' => '4', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'AGYA ',
                'car_brand_id' => '5', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'AVANZA',
                'car_brand_id' => '5', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'CAMRY',
                'car_brand_id' => '5', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'YARIS',
                'car_brand_id' => '5', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'XPANDER ',
                'car_brand_id' => '6', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'MIRAGE ',
                'car_brand_id' => '6', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'PAJERO SPORT ',
                'car_brand_id' => '6', 'created_at' => \Illuminate\Support\Carbon::now(), 'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);
    }
}
