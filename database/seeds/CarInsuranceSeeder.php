<?php

use Illuminate\Database\Seeder;

class CarInsuranceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_insurances')->insert([
            [
                'name' => 'AXA',
                'logo' => '/media/image/partner/axa_logo.png',
                'admin_fee' => 200000,
                'eqvet_comp' => 0.22,
                'eqvet_tlo' => 0.20,
                'tsfwd_comp' => 0.18,
                'tsfwd_tlo' => 0.21,
                'srcc_comp' => 0.24,
                'srcc_tlo' => 0.22,
                'ts_comp' => 0.26,
                'ts_tlo' => 0.20,
                'tpl' => 0.17,
                'pa_driver' => 0.20,
                'pa_passenger' => 0.21,
                'atpm' => 0.18,
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'Sinarmas',
                'logo' => '/media/image/partner/sinarmas_logo.png',
                'admin_fee' => 190000,
                'eqvet_comp' => 0.21,
                'eqvet_tlo' => 0.21,
                'tsfwd_comp' => 0.19,
                'tsfwd_tlo' => 0.18,
                'srcc_comp' => 0.22,
                'srcc_tlo' => 0.21,
                'ts_comp' => 0.25,
                'ts_tlo' => 0.24,
                'tpl' => 0.23,
                'pa_driver' => 0.22,
                'pa_passenger' => 0.22,
                'atpm' => 0.20,
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'ACA',
                'logo' => '/media/image/partner/aca_logo.jpg',
                'admin_fee' => 185000,
                'eqvet_comp' => 0.21,
                'eqvet_tlo' => 0.20,
                'tsfwd_comp' => 0.23,
                'tsfwd_tlo' => 0.18,
                'srcc_comp' => 0.22,
                'srcc_tlo' => 0.20,
                'ts_comp' => 0.25,
                'ts_tlo' => 0.22,
                'tpl' => 0.21,
                'pa_driver' => 0.17,
                'pa_passenger' => 0.19,
                'atpm' => 0.18,
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Avrist',
                'logo' => '/media/image/partner/avrist_logo.png',
                'admin_fee' => 210000,
                'eqvet_comp' => 0.21,
                'eqvet_tlo' => 0.24,
                'tsfwd_comp' => 0.22,
                'tsfwd_tlo' => 0.23,
                'srcc_comp' => 0.22,
                'srcc_tlo' => 0.25,
                'ts_comp' => 0.20,
                'ts_tlo' => 0.21,
                'tpl' => 0.18,
                'pa_driver' => 0.16,
                'pa_passenger' => 0.16,
                'atpm' => 0.19,
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'MPM',
                'logo' => '/media/image/partner/mpm_logo.png',
                'admin_fee' => 220000,
                'eqvet_comp' => 0.23,
                'eqvet_tlo' => 0.24,
                'tsfwd_comp' => 0.26,
                'tsfwd_tlo' => 0.28,
                'srcc_comp' => 0.24,
                'srcc_tlo' => 0.25,
                'ts_comp' => 0.19,
                'ts_tlo' => 0.23,
                'tpl' => 0.18,
                'pa_driver' => 0.17,
                'pa_passenger' => 0.18,
                'atpm' => 0.20,
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Reliance',
                'logo' => '/media/image/partner/reliance_logo.png',
                'admin_fee' => 188000,
                'eqvet_comp' => 0.24,
                'eqvet_tlo' => 0.26,
                'tsfwd_comp' => 0.23,
                'tsfwd_tlo' => 0.25,
                'srcc_comp' => 0.21,
                'srcc_tlo' => 0.25,
                'ts_comp' => 0.19,
                'ts_tlo' => 0.24,
                'tpl' => 0.17,
                'pa_driver' => 0.18,
                'pa_passenger' => 0.18,
                'atpm' => 0.16,
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);

        DB::table('car_base_rates')->insert([
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '1',
                'value' => 3.9,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '2',
                'value' => 3.3,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '3',
                'value' => 2.7,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '1',
                'value' => 2.9,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '2',
                'value' => 2.6,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '3',
                'value' => 2.8,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '1',
                'value' => 2.5,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '2',
                'value' => 2.2,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '3',
                'value' => 2,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '1',
                'value' => 1.40,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '2',
                'value' => 1.40,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '3',
                'value' => 1.35,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '1',
                'value' => 1.25,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '2',
                'value' => 1.25,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '3',
                'value' => 1.25,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '1',
                'value' => 0.51,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '2',
                'value' => 0.68,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '3',
                'value' => 0.6,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '1',
                'value' => 0.67,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '2',
                'value' => 0.51,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '3',
                'value' => 0.46,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '1',
                'value' => 0.43,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '2',
                'value' => 0.42,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '3',
                'value' => 0.36,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '1',
                'value' => 0.28,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '2',
                'value' => 0.28,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '3',
                'value' => 0.26,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '1',
                'value' => 0.24,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '2',
                'value' => 0.24,
                'car_insurance_id' => 1
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '3',
                'value' => 0.24,
                'car_insurance_id' => 1
            ],
            //sinarmas
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '1',
                'value' => 3.85,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '2',
                'value' => 3.4,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '3',
                'value' => 2.74,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '1',
                'value' => 2.81,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '2',
                'value' => 2.61,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '3',
                'value' => 2.76,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '1',
                'value' => 2.32,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '2',
                'value' => 2.14,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '3',
                'value' => 1.9,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '1',
                'value' => 1.25,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '2',
                'value' => 1.25,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '3',
                'value' => 1.3,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '1',
                'value' => 1.15,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '2',
                'value' => 1.15,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '3',
                'value' => 1.15,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '1',
                'value' => 0.49,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '2',
                'value' => 0.67,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '3',
                'value' => 0.56,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '1',
                'value' => 0.65,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '2',
                'value' => 0.5,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '3',
                'value' => 0.47,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '1',
                'value' => 0.46,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '2',
                'value' => 0.4,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '3',
                'value' => 0.35,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '1',
                'value' => 0.275,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '2',
                'value' => 0.275,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '3',
                'value' => 0.28,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '1',
                'value' => 0.21,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '2',
                'value' => 0.21,
                'car_insurance_id' => 2
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '3',
                'value' => 0.21,
                'car_insurance_id' => 2
            ], //ACA
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '1',
                'value' => 3.82,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '2',
                'value' => 3.26,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '3',
                'value' => 2.52,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '1',
                'value' => 2.67,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '2',
                'value' => 2.47,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '3',
                'value' => 2.69,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '1',
                'value' => 2.18,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '2',
                'value' => 2.08,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '3',
                'value' => 1.79,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '1',
                'value' => 1.20,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '2',
                'value' => 1.20,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '3',
                'value' => 1.14,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '1',
                'value' => 1.05,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '2',
                'value' => 1.05,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '3',
                'value' => 1.05,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '1',
                'value' => 0.47,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '2',
                'value' => 0.65,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '3',
                'value' => 0.51,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '1',
                'value' => 0.63,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '2',
                'value' => 0.44,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '3',
                'value' => 0.44,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '1',
                'value' => 0.41,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '2',
                'value' => 0.38,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '3',
                'value' => 0.29,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '1',
                'value' => 0.25,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '2',
                'value' => 0.25,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '3',
                'value' => 0.23,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '1',
                'value' => 0.20,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '2',
                'value' => 0.20,
                'car_insurance_id' => 3
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '3',
                'value' => 0.20,
                'car_insurance_id' => 3
            ], //Avrist
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '1',
                'value' => 3.82,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '2',
                'value' => 3.26,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '3',
                'value' => 2.52,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '1',
                'value' => 2.67,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '2',
                'value' => 2.47,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '3',
                'value' => 2.69,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '1',
                'value' => 2.18,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '2',
                'value' => 2.08,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '3',
                'value' => 1.79,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '1',
                'value' => 1.20,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '2',
                'value' => 1.20,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '3',
                'value' => 1.14,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '1',
                'value' => 1.05,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '2',
                'value' => 1.05,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '3',
                'value' => 1.05,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '1',
                'value' => 0.47,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '2',
                'value' => 0.65,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '3',
                'value' => 0.51,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '1',
                'value' => 0.63,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '2',
                'value' => 0.44,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '3',
                'value' => 0.44,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '1',
                'value' => 0.41,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '2',
                'value' => 0.38,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '3',
                'value' => 0.29,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '1',
                'value' => 0.25,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '2',
                'value' => 0.25,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '3',
                'value' => 0.23,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '1',
                'value' => 0.20,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '2',
                'value' => 0.20,
                'car_insurance_id' => 4
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '3',
                'value' => 0.20,
                'car_insurance_id' => 4
            ], //MPM
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '1',
                'value' => 3.91,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '2',
                'value' => 3.31,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '3',
                'value' => 2.68,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '1',
                'value' => 2.76,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '2',
                'value' => 2.58,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '3',
                'value' => 2.72,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '1',
                'value' => 2.22,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '2',
                'value' => 2.16,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '3',
                'value' => 1.86,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '1',
                'value' => 1.38,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '2',
                'value' => 1.38,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '3',
                'value' => 1.26,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '1',
                'value' => 1.22,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '2',
                'value' => 1.22,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '3',
                'value' => 1.22,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '1',
                'value' => 0.5,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '2',
                'value' => 0.675,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '3',
                'value' => 0.585,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '1',
                'value' => 0.66,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '2',
                'value' => 0.48,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '3',
                'value' => 0.46,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '1',
                'value' => 0.42,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '2',
                'value' => 0.41,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '3',
                'value' => 0.335,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '1',
                'value' => 0.27,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '2',
                'value' => 0.27,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '3',
                'value' => 0.25,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '1',
                'value' => 0.22,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '2',
                'value' => 0.22,
                'car_insurance_id' => 5
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '3',
                'value' => 0.22,
                'car_insurance_id' => 5
            ], //Reliance
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '1',
                'value' => 3.82,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '2',
                'value' => 3.26,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '1',
                'region' => '3',
                'value' => 2.52,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '1',
                'value' => 2.67,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '2',
                'value' => 2.47,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '2',
                'region' => '3',
                'value' => 2.69,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '1',
                'value' => 2.18,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '2',
                'value' => 2.08,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '3',
                'region' => '3',
                'value' => 1.79,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '1',
                'value' => 1.20,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '2',
                'value' => 1.20,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '4',
                'region' => '3',
                'value' => 1.14,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '1',
                'value' => 1.05,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '2',
                'value' => 1.05,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'comp',
                'category' => '5',
                'region' => '3',
                'value' => 1.05,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '1',
                'value' => 0.47,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '2',
                'value' => 0.65,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '1',
                'region' => '3',
                'value' => 0.51,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '1',
                'value' => 0.63,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '2',
                'value' => 0.44,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '2',
                'region' => '3',
                'value' => 0.44,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '1',
                'value' => 0.41,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '2',
                'value' => 0.38,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '3',
                'region' => '3',
                'value' => 0.29,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '1',
                'value' => 0.25,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '2',
                'value' => 0.25,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '4',
                'region' => '3',
                'value' => 0.23,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '1',
                'value' => 0.20,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '2',
                'value' => 0.20,
                'car_insurance_id' => 6
            ],
            [
                'type' => 'tlo',
                'category' => '5',
                'region' => '3',
                'value' => 0.20,
                'car_insurance_id' => 6
            ],
        ]);
    }
}
