<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_insurances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('name');
            $table->String('logo');
            $table->integer('admin_fee');
            $table->float('rate_flexas_class1', 6, 3);
            $table->float('rate_flexas_class2', 6, 3);
            $table->float('rate_flexas_class3', 6, 3);
            $table->float('rate_rsmdcc', 6, 3);
            $table->float('rate_other', 6, 3);
            $table->float('rate_tsfwd', 6, 3);
            $table->float('rate_eqvet_zone1', 6, 3);
            $table->float('rate_eqvet_zone2', 6, 3);
            $table->float('rate_eqvet_zone3', 6, 3);
            $table->float('rate_eqvet_zone4', 6, 3);
            $table->float('rate_eqvet_zone5', 6, 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_insurances');
    }
}
