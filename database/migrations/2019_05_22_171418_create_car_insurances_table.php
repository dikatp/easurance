<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_insurances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('name');
            $table->String('logo');
            $table->integer('admin_fee');
            $table->float('eqvet_comp', 6, 3);
            $table->float('eqvet_tlo', 6, 3);
            $table->float('tsfwd_comp', 6, 3);
            $table->float('tsfwd_tlo', 6, 3);
            $table->float('srcc_comp', 6, 3);
            $table->float('srcc_tlo', 6, 3);
            $table->float('ts_comp', 6, 3);
            $table->float('ts_tlo', 6, 3);
            $table->float('tpl', 6, 3);
            $table->float('pa_driver', 6, 3);
            $table->float('pa_passenger', 6, 3);
            $table->float('atpm', 6, 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_insurances');
    }
}
