<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('duration_from');
            $table->integer('duration_to');
            $table->integer('price');
            $table->unsignedInteger('travel_package_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_rates');
    }
}
