<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('car_insurance_car_workshop', function (Blueprint $table) {
            $table->unsignedInteger('car_insurance_id');
            $table->unsignedInteger('car_workshop_id');
            $table->timestamps();
        });

        Schema::create('car_benefit_car_insurance', function (Blueprint $table) {
            $table->unsignedInteger('car_insurance_id');
            $table->unsignedInteger('car_benefit_id');
            $table->timestamps();
        });

        Schema::create('property_benefit_property_insurance', function (Blueprint $table) {
            $table->unsignedInteger('property_insurance_id');
            $table->unsignedInteger('property_benefit_id');
            $table->timestamps();
        });

        Schema::create('travel_benefit_travel_insurance', function (Blueprint $table) {
            $table->unsignedInteger('travel_insurance_id');
            $table->unsignedInteger('travel_benefit_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('car_insurance_car_workshop');
        Schema::dropIfExists('car_benefit_car_insurance');
        Schema::dropIfExists('property_benefit_property_insurance');
        Schema::dropIfExists('travel_benefit_travel_insurance');
    }
}
