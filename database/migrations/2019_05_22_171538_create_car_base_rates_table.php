<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarBaseRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_base_rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('type');
            $table->tinyInteger('category');
            $table->tinyInteger('region');
            $table->float('value', 6, 3);
            $table->unsignedInteger('car_insurance_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_base_rates');
    }
}
